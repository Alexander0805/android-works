package co.za.dfmtechnologies.pestmonitoring;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;



import com.google.gson.JsonParser;
import com.loopj.android.http.HttpGet;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.za.dfmtechnologies.R;
import co.za.dfmtechnologies.pestmonitoring.realm.DfmRealm;
import co.za.dfmtechnologies.pestmonitoring.ui.login.LoginActivity;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class Setting extends AppCompatActivity {
    private List list;
    private List list2;
    private List list3;
    private List list4;
    private List pest_list ;
    SprayblocksHelper sDb;
    String al;
    String results;
    String sprayURL,pestURL,monURL;
    String CustID;
    String fVal;
    String pa;
    Switch switch_theme;
    SharedPreferences prf,pref;
    Button logout,sync,upload;
    TextView user;
    Intent intent;
    int dt ;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    int id = 1;
    ImageView  home;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.settings);



      sDb = new SprayblocksHelper(this);
      list = new ArrayList<Integer>();
      pest_list = new ArrayList<String>();

      list4 = new ArrayList<String>();

    logout = findViewById(R.id.button6);
    prf = getSharedPreferences("Login", MODE_PRIVATE);
      pref = getSharedPreferences("user_details", MODE_PRIVATE);
    intent = new Intent(Setting.this, MainActivity.class);
    al = pref.getString("usn", null);
    pa = pref.getString("pass", null);

    CustID = pref.getString("cust",null);
    user= findViewById(R.id.textView14);
    user.setText(al);
    //buttonView=findViewById(R.id.textView10);




    logout.setOnClickListener(new View.OnClickListener() {@Override
    public void onClick(View v) { SharedPreferences.Editor editor = prf.edit();
    editor.clear();
    editor.commit();
    removeSprayBlocks();
        DfmRealm dfmRealm = DfmRealm.getInstance();
        dfmRealm.clearAllData();
    Setting setting = Setting.this;
    setting.startActivity(setting.intent);
    Intent intent = new Intent((Context)Setting.this, LoginActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    Setting.this.startActivity(intent);
    }
    });
    sync=findViewById(R.id.button7);
      sync.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              networkcheck();
          }
      });

      home = (ImageView) findViewById(R.id.imageView5);
      home.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {


              Intent intent = new Intent(Setting.this, activity_pest_monitoring.class);
            Setting .this.startActivity(intent);
          }
      });


  }

    // Class with extends AsyncTask class
    private class LongOperation extends AsyncTask<String, Void, Void> {

        private final HttpClient Client = new DefaultHttpClient();
        private String Content;
        private String Error = null;
        private ProgressDialog Dialog = new ProgressDialog(Setting.this);


        protected void onPreExecute() {
            // NOTE: You can call UI Element here.

            //UI Element

            Dialog.setMessage("Syncing...");
            Dialog.show();


        }

        // Call after onPreExecute method
        protected Void doInBackground(String... urls) {
            try {

                // Call long running operations here (perform background computation)
                // NOTE: Don't call UI Element here.

                // Server url call by GET method
                HttpGet httpget = new HttpGet(urls[0]);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                Content = Client.execute(httpget, responseHandler);

            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            // NOTE: You can call UI Element here.

            // Close progress dialog
            Dialog.dismiss();

            if (Error != null) {

                Toast.makeText(Setting.this,"Sync Failed Please Check Internet Connection",Toast.LENGTH_SHORT).show();

                // user.setText("Output : " + Error);

            } else {

                results =String.valueOf(Content);

                String[] row = results.split("\\r\\n|\\n|\\r");

                String[] rod = row[0].split("\\|");

                    fVal=(rod[0]);



                if(fVal.equals("MB4000SprayBlockID")){
                    syncSprayBlocks();
                }else if((fVal.equals("Pest"))){
                    syncPest();
                }else if(fVal.equals("MB4000PestMonitoringPointID")) {
                    syncMoni();
                }
            }
        }

    }
    public boolean removeSprayBlocks() {
        // SQLiteDatabase sdb = sDb.getWritableDatabase();
        String[] list = this.databaseList();
        for(String name : list){
                this.deleteDatabase(name);
        }

         return true;
    }

    private void networkcheck () {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            connected = true;
        } else
            connected = false;

        if (connected == true) {
            SQLiteDatabase sdb = sDb.getWritableDatabase();
            sdb.delete(sDb.TABLE_NAME,null, null);
            sdb.delete(sDb.TABLE_NAME1,null, null);
            sdb.delete(sDb.TABLE_NAME2,null, null);
            sdb.close();


            sprayURL ="http://probe.dfmsoftware.co.za/WebServices/WSCustomers.ashx?Action=GetMB4000SPRAYBLOCK&Username="+al+"&userPassword="+pa+"&CustomerID="+CustID;
            pestURL ="http://probe.dfmsoftware.co.za/WebServices/WSCustomers.ashx?Action=GetMB4000PESTS&Username="+al+"&userPassword="+pa+"&CustomerID="+CustID;
            monURL ="http://probe.dfmsoftware.co.za/WebServices/WSCustomers.ashx?Action=GetMB4000PestMonitoringPoints&Username="+al+"&userPassword="+pa+"&CustomerID="+CustID;

            new LongOperation().execute(sprayURL);
            new LongOperation().execute(pestURL);
            new LongOperation().execute(monURL);

        } else
            Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).show();
    }
    public boolean syncSprayBlocks() {

         String[] row = results.split("\\r\\n|\\n|\\r");

         for (int i = 1; i < row.length; i++) {

             String[] rod = row[i].split("\\|");
             for (int j = 0; j < rod.length; j++) {
                 pest_list.add(rod[j]);

             }
             if (pest_list.size() > 0) {
                 sDb.insertData(rod[0], rod[1], rod[2], rod[3], rod[4], rod[5], rod[6], rod[7], rod[8], rod[9], rod[10], rod[11], rod[12], rod[13], rod[14], rod[15], rod[16], rod[17], rod[18], rod[19], rod[20], rod[21], rod[22]);

                 // Toast.makeText(MainActivity.this,""+pest_list.get(i-1),Toast.LENGTH_SHORT).show();
             }
         }
        sDb.close();
         Toast.makeText(Setting.this, "Data has been synced", Toast.LENGTH_SHORT).cancel();

         return true;
    }

    public boolean syncPest() {

        String[] row = results.split("\\r\\n|\\n|\\r");

        for (int i = 1; i < row.length; i++) {

            String[] rod = row[i].split("\\|");
            for (int j = 0; j < rod.length; j++) {
                list.add(rod[j]);

            }
            if (list.size() > 0) {

                sDb.insertPData(rod[0], rod[1], rod[2], rod[3], rod[4], rod[5], rod[6], rod[7], rod[8], rod[9], rod[10], rod[11]);
                // Toast.makeText(MainActivity.this,""+pest_list.get(i-1),Toast.LENGTH_SHORT).show();
            }
        }
        sDb.close();
        Toast.makeText(Setting.this, "Data has been synced", Toast.LENGTH_SHORT).cancel();

        return true;
    }


    public boolean syncMoni() {

        String[] row = results.split("\\r\\n|\\n|\\r");

        for (int i = 1; i < row.length; i++) {

            String[] rod = row[i].split("\\|");
            for (int j = 0; j < rod.length; j++) {
                list4.add(rod[j]);

            }
            if (list4.size() > 1) {
                sDb.insertMdata(rod[0], rod[1], rod[2], rod[3], rod[4], rod[5], rod[6], rod[7], rod[8]);
                // Toast.makeText(MainActivity.this,""+pest_list.get(i-1),Toast.LENGTH_SHORT).show();
            }
        }
        sDb.close();
        Toast.makeText(Setting.this, "Data has been synced", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Setting.this, activity_pest_monitoring.class);
        Setting .this.startActivity(intent);
        return true;
    }



}
