package co.za.dfmtechnologies.pestmonitoring;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;
import co.za.dfmtechnologies.R;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {
  private ItemClickListener mClickListener;
  
  private List<String> mData;
  
  private LayoutInflater mInflater;
  
  MyRecyclerViewAdapter(Context paramContext, List<String> paramList) {
    this.mInflater = LayoutInflater.from(paramContext);
    this.mData = paramList;
  }
  
  String getItem(int paramInt) {
    return this.mData.get(paramInt);
  }
  
  public int getItemCount() {
    return this.mData.size();
  }
  
  public void onBindViewHolder(ViewHolder paramViewHolder, int paramInt) {
    String str = this.mData.get(paramInt);
    paramViewHolder.myTextView.setText(str);
  }
  
  public ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt) {
    return new ViewHolder(this.mInflater.inflate(R.layout.recyclerview_row, paramViewGroup, false));
  }
  
  void setClickListener(ItemClickListener paramItemClickListener) {
    this.mClickListener = paramItemClickListener;
  }
  
  public static interface ItemClickListener {
    void onItemClick(View param1View, int param1Int);
  }
  
  public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView myTextView;
    EditText myEditText;
    
    ViewHolder(View param1View) {
      super(param1View);
      this.myTextView = (TextView)param1View.findViewById(R.id.ItemName);
      this.myEditText = (EditText) param1View.findViewById(R.id.number);
      param1View.setOnClickListener(this);
    }
    
    public void onClick(View param1View) {
      if (MyRecyclerViewAdapter.this.mClickListener != null)
        MyRecyclerViewAdapter.this.mClickListener.onItemClick(param1View, getAdapterPosition()); 
    }
  }
}

