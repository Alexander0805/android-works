package co.za.dfmtechnologies.pestmonitoring;


import java.util.jar.Attributes;

public class Names {
  private String MB4000PestMonitoringReadings;
  private String PestMonitoringReadingDate;
  private String PestMonitoringPointID;
  private String Reading;
  private String MonitoringPoints;
  private String Percentage;
  private String FarmID;
  private String CustomerID;
  private String Active;

  public Names(String MB4000PestMonitoringReadings, String PestMonitoringReadingDate, String PestMonitoringPointID, String Reading, String MonitoringPoints, String Percentage, String FarmID, String CustomerID, String Active) {
    this.MB4000PestMonitoringReadings = MB4000PestMonitoringReadings;
    this.PestMonitoringReadingDate = PestMonitoringReadingDate;
    this.PestMonitoringPointID = PestMonitoringPointID;
    this.Reading = Reading;
    this.MonitoringPoints = MonitoringPoints;
    this.Percentage = Percentage;
    this.FarmID = FarmID;
    this.CustomerID = CustomerID;
    this.Active = Active;
  }

  public String getMB4000PestMonitoringReadings() {
    return MB4000PestMonitoringReadings;
  }

  public String getPestMonitoringReadingDate() {
    return PestMonitoringReadingDate;
  }

  public String getPestMonitoringPointID() {
    return PestMonitoringPointID;
  }

  public String getReading() {
    return Reading;
  }

  public String getMonitoringPoints() {
    return MonitoringPoints;
  }

  public String getPercentage() {
    return Percentage;
  }

  public String getFarmID() {
    return FarmID;
  }

  public String getCustomerID() {
    return CustomerID;
  }

  public String getActive() {
    return Active;
  }
}