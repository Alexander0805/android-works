package co.za.dfmtechnologies.pestmonitoring;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import co.za.dfmtechnologies.R;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class blockView extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {
  private static final String TAG = "0";
  MyRecyclerViewAdapter adapter;
  List Block;
  Intent intent;
  String met;
  String monP;
  String spid;
  String mpid;
  SharedPreferences pref;
  ImageView setting, home;
  private static final int REQUEST_LOCATION = 1;
  SprayblocksHelper sDb;
  String latitude, longitude;
  LocationManager locationManager;
  Button btblock;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.monitoring_points);
    getMonitoringpoints();


    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
      showAlert();

    } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

      getLocation();

    }


    sDb = new SprayblocksHelper(this);

    setting = findViewById(R.id.imageView6);
    setting.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {


        //   for (int i = 1; i < getAllFriends().size(); i++) {
//

        Intent intent = new Intent(blockView.this, Setting.class);
        blockView.this.startActivity(intent);

      }
    });
    home = (ImageView) findViewById(R.id.imageView5);
    home.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(blockView.this, activity_pest_monitoring.class);
        blockView.this.startActivity(intent);
      }
    });

    btblock = findViewById(R.id.button8);
    btblock.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        locationManager.removeUpdates(locationListenerGPS);
        locationManager.removeUpdates(locationListenerNetwork);
        Intent intent = new Intent(blockView.this, Blocks.class);
        blockView.this.startActivity(intent);


      }
    });


    pref = getSharedPreferences("user_details", MODE_PRIVATE);
    intent = new Intent(blockView.this, Blocks.class);
    spid = pref.getString("spid", null);
    met = this.pref.getString("method", null);


    RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvMp);
    recyclerView.setLayoutManager((RecyclerView.LayoutManager) new LinearLayoutManager((Context) this));
    MyRecyclerViewAdapter myRecyclerViewAdapter = new MyRecyclerViewAdapter((Context) this, getMonitoringpoints());
    adapter = myRecyclerViewAdapter;
    myRecyclerViewAdapter.setClickListener(this);
    recyclerView.setAdapter(adapter);
    recyclerView.addItemDecoration((RecyclerView.ItemDecoration) new DividerItemDecoration(recyclerView.getContext(), 1));

  }

  public void onItemClick(View paramView, int paramInt) {
    SharedPreferences.Editor editor = pref.edit();
    monP = adapter.getItem(paramInt);
    // editor.putString("monpid",getMonitoringpointsid().get(0)).apply();
    // Log.d("sprayblockid",getMonitoringpointsid().get(0));
    editor.putString("mp", adapter.getItem(paramInt)).apply();
    editor.commit();


    if (met.equals("0")) {
      locationManager.removeUpdates(locationListenerGPS);
      locationManager.removeUpdates(locationListenerNetwork);
      startActivity(new Intent(blockView.this, traps.class));
    } else {
      locationManager.removeUpdates(locationListenerGPS);
      locationManager.removeUpdates(locationListenerNetwork);
      startActivity(new Intent(blockView.this, scouting.class));

    }


  }

  private boolean checkLocation() {
    if (!isLocationEnabled()) {
      showAlert();
    } else {
      getLocation();
    }
    return isLocationEnabled();
  }

  private boolean isLocationEnabled() {
    return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
  }

  private void showAlert() {
    final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
    dialog.setTitle("Enable Location")
            .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                    "use this app")
            .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
              }
            })
            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface paramDialogInterface, int paramInt) {
              }
            });
    dialog.show();
  }

  private void getLocation() {
    if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
            (this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
    } else {
      locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 50, 0, locationListenerNetwork);
      locationManager.requestLocationUpdates(
              LocationManager.GPS_PROVIDER, 0, 0, locationListenerGPS);


      Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
      Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

      Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
      if (location != null) {
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 50, 0, locationListenerNetwork);
        double latti = location.getLatitude();
        double longi = location.getLongitude();
        latitude = String.valueOf(latti);
        longitude = String.valueOf(longi);


      } else if (location1 != null) {
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 1000, 5, locationListenerGPS);
        double latti = location1.getLatitude();
        double longi = location1.getLongitude();
        latitude = String.valueOf(latti);
        longitude = String.valueOf(longi);


      } else if (location2 != null) {
        locationManager.requestLocationUpdates(
                LocationManager.PASSIVE_PROVIDER, 50, 0, locationListenerGPS);
        double latti = location2.getLatitude();
        double longi = location2.getLongitude();
        latitude = String.valueOf(latti);
        longitude = String.valueOf(longi);


      }
    }
  }

  public ArrayList<String> getMonitoringpoints() {


    ArrayList<String> Block = new ArrayList<>();
    try {
      SQLiteDatabase db = sDb.getWritableDatabase();


      //String sql ="Select PestMonitoringPoint From MonitoringPoints where SprayBlockID='"+spid+"'";
      String sql = "SELECT O.Number,I.PestMonitoringPoint ,O.MB4000SprayBlockID , P.Pest,P.LoggingMethod FROM [Sprayblocks] O JOIN MonitoringPoints I ON O.MB4000SprayBlockID = I.SprayBlockID JOIN Pest P ON P.Pest = I.Pest   where P.LoggingMethod ='" + met + "' and I.SprayBlockID='" + spid + "' ORDER BY I.PestMonitoringPoint";
      Cursor cursor = db.rawQuery(sql, null);

      while (cursor.moveToNext()) {
        if (Block.contains(cursor.getString(cursor.getColumnIndex("PestMonitoringPoint")))) {
        } else {
          Block.add(cursor.getString(cursor.getColumnIndex("PestMonitoringPoint")));
        }

      }
      cursor.close();
    } catch (Exception ex) {
      Log.e(TAG, "Error in getting points " + ex.toString());
    }

    return Block;
  }

  private final LocationListener locationListenerGPS = new LocationListener() {
    public void onLocationChanged(Location location) {
      double latti = location.getLatitude();
      double longi = location.getLongitude();
      latitude = String.valueOf(latti);
      longitude = String.valueOf(longi);

      runOnUiThread(new Runnable() {
        @Override
        public void run() {


          //Toast.makeText(scouting.this, "Location Ready", Toast.LENGTH_SHORT).show();
        }
      });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
  };


  private final LocationListener locationListenerNetwork = new LocationListener() {
    public void onLocationChanged(Location location) {

      location.getAccuracy();
      double latti = location.getLatitude();
      double longi = location.getLongitude();
      latitude = String.valueOf(latti);
      longitude = String.valueOf(longi);

      runOnUiThread(new Runnable() {
        @Override
        public void run() {

          //Toast.makeText(scouting.this, "Network Provider update", Toast.LENGTH_SHORT).show();
        }
      });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
  };


  @Override
  public void startActivityForResult(Intent intent, int requestCode) {
    if (intent == null) {
      intent = new Intent();
    }
    super.startActivityForResult(intent, requestCode);
  }


  boolean doubleBackToExitPressedOnce = false;

  @Override
  public void onBackPressed() {
    if (doubleBackToExitPressedOnce) {
      super.onBackPressed();
      return;
    }

    this.doubleBackToExitPressedOnce = true;
    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

    new Handler().postDelayed(new Runnable() {

      @Override
      public void run() {
        doubleBackToExitPressedOnce = false;
      }
    }, 2000);
  }
}



