package co.za.dfmtechnologies.pestmonitoring.rest;

import android.support.annotation.NonNull;


/**
 * Grant Type
 */
public enum GrantType {

    PASSWORD("password"),
    REFRESH_TOKEN("refresh_token");

    String value;

    GrantType(@NonNull String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
