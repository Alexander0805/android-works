package co.za.dfmtechnologies.pestmonitoring;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import co.za.dfmtechnologies.pestmonitoring.realm.DfmRealm;
import co.za.dfmtechnologies.pestmonitoring.rest.DfmRetrofit;
import io.fabric.sdk.android.BuildConfig;
import io.fabric.sdk.android.Fabric;



/**
 * DFM Application
 */
public class DfmApplication extends Application {

    public void onCreate() {
        super.onCreate();

        if(!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }

        DfmRealm dfmRealm = DfmRealm.getInstance();
        dfmRealm.initRealm(this.getApplicationContext());

        DfmRetrofit dfmRetrofit = DfmRetrofit.getInstance();
        dfmRetrofit.init(dfmRealm);
    }
}
