package co.za.dfmtechnologies.pestmonitoring.rest;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;


import co.za.dfmtechnologies.BuildConfig;
import co.za.dfmtechnologies.pestmonitoring.realm.DfmRealm;
import co.za.dfmtechnologies.pestmonitoring.realm.Logger;
import co.za.dfmtechnologies.pestmonitoring.realm.model.User;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * DFM Retrofit
 */
public class DfmRetrofit implements DfmEndpoint {

    private static final String TAG = DfmRetrofit.class.getSimpleName();


    private static DfmRetrofit INSTANCE;

    public static DfmRetrofit getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new DfmRetrofit();
        }

        return INSTANCE;
    }

    private DfmEndpoint dfmEndpoint;
    private DfmRealm dfmRealm;

    private DfmRetrofit() {

    }

    public void init(@NonNull DfmRealm dfmRealm) {

        this.dfmRealm = dfmRealm;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        this.dfmEndpoint = retrofit.create(DfmEndpoint.class);
    }

    @Override
    public Observable<User> authenticateUser(String grantType, String userName, String password) {
        return Observable.create(e -> this.dfmEndpoint.authenticateUser(grantType, userName, password)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(user -> {
                    dfmRealm.addOrUpdateUser(user);
                    e.onNext(user);
                    e.onComplete();
                }, throwable -> {
                    Logger.e(TAG, "Authenticate User", throwable);
                    e.onError(throwable);
                }));
    }

    @Override
    public Observable<User> refreshAuthorizationToken(String grantType, String refreshToken) {
        return Observable.create(e -> this.dfmEndpoint.refreshAuthorizationToken(grantType, refreshToken)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(user -> {
                    e.onNext(user);
                    e.onComplete();
                }, e::onError));
    }

    private String getAuthorizationHeader() {
        return String.format("bearer %s", this.dfmRealm.getCurrentUser().getAccessToken());
    }

    @SuppressLint("CheckResult")
    private Observable<Boolean> refreshAuthTokenIfNeeded() {

        return Observable.create(e -> {
            User user = this.dfmRealm.getCurrentUser();

            if((System.currentTimeMillis() - user.getTokenExpireTime()) > 0) {
                this.refreshAuthorizationToken(GrantType.REFRESH_TOKEN.getValue(), user.getRefreshToken())
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .subscribe(refreshUser -> {
                            dfmRealm.addOrUpdateUser(refreshUser);
                            e.onNext(true);
                            e.onComplete();
                        }, e::onError);
                return;
            }

            e.onNext(true);
            e.onComplete();
        });
    }
}
