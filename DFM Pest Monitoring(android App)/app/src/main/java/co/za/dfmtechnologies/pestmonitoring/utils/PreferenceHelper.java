package co.za.dfmtechnologies.pestmonitoring.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import co.za.dfmtechnologies.BuildConfig;


/**
 * Preference Helper
 */
public class PreferenceHelper {

    private static final String KEY_GRAPH_SELECTION_FILTER = "graph.selection.filter";

    private static PreferenceHelper INSTANCE;

    public static PreferenceHelper getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            INSTANCE = new PreferenceHelper(context);
        }

        return INSTANCE;
    }

    private final SharedPreferences preferences;
    private final Gson gson;

    public PreferenceHelper(@NonNull Context context) {
        this.gson = new Gson();
        preferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }


    @Nullable


    private void saveStringConfig(@NonNull String key, @Nullable String value) {
        this.preferences.edit().putString(key, value).apply();
    }

    @Nullable
    private String getStringConfig(@NonNull String key) {
        return this.preferences.getString(key, null);
    }
}
