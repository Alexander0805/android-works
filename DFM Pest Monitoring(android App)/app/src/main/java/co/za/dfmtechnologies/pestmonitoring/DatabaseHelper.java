package co.za.dfmtechnologies.pestmonitoring;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



  public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DFMPestmonitoring.DB";
    public static final String TABLE_NAME = "PestReading";
    public static final String TABLE_NAME1 = "PestReading1";
    public static final String COL1="MB4000PestMonitoringReadingsID";
    public static final String COL2="PestMonitoringReadingDate";
    public static final String COL3="PestMonitoringPointID";
    public static final String COL4="Reading";
    public static final String COL5="MonitoringPoints";
    public static final String COL7="ReadingLat";
    public static final String COL8="ReadingLon";
    public static final String COL9="ReadingPest";
    public static final String COL10="ReadingUnlistedPest";
    public static final String COL11="ReadingImage";
    public static final String COL13="CustomerID";
    public static final String COL14="Active";




    public DatabaseHelper(Context context) {
      super(context, DATABASE_NAME, null, 1);
      SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL("create table " + TABLE_NAME +"(MB4000PestMonitoringReadingsID INTEGER PRIMARY KEY AUTOINCREMENT ,PestMonitoringReadingDate DATETIME,PestMonitoringPointID TEXT,Reading TEXT,MonitoringPoints TEXT,ReadingLat TEXT,ReadingLon TEXT,ReadingPest TEXT,PestReadingSprayBlockID TEXT, CustomerID TEXT,Active TEXT)");
      db.execSQL("create table " + TABLE_NAME1 +"(MB4000PestMonitoringReadingsID INTEGER PRIMARY KEY AUTOINCREMENT ,PestMonitoringReadingDate DATETIME,PestMonitoringPointID TEXT,Reading TEXT,MonitoringPoints TEXT,ReadingLat TEXT,ReadingLon TEXT,ReadingPest TEXT,ReadingUnlistedPest TEXT,ReadingImage TEXT,PestReadingSprayBlockID TEXT , CustomerID TEXT,Active TEXT,PestMonitoringPointName TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
      db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME1);
      onCreate(db);
    }
// String pestMonitoringReadingDate
    public boolean insertData(String pestMonitoringReadingDate,String pestMonitoringPointID,String reading,
                              String monitoringPoints,String pestReadingSprayBlockID,String customerID,String active ,String readinglat,String readinglon,String readingpest) {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues contentValues = new ContentValues();
     // contentValues.put(COL1 ,mB4000PestMonitoringReadingsID);
      contentValues.put(COL2, pestMonitoringReadingDate);
      contentValues.put(COL3 ,pestMonitoringPointID);
      contentValues.put(COL4 ,reading);
      contentValues.put(COL5, monitoringPoints);
      contentValues.put(COL7, readinglat);
      contentValues.put(COL8, readinglon);
      contentValues.put(COL9,readingpest);
      contentValues.put("PestReadingSprayBlockID",pestReadingSprayBlockID);
      contentValues.put(COL13, customerID);
      contentValues.put(COL14, active);


      long result = db.insert(TABLE_NAME,null, contentValues);
      if(result == -1)
        return false;
      else
        return true;
    }

    public boolean insertUlistedPest(String pestMonitoringReadingDate,String pestMonitoringPointID,String reading,
                              String monitoringPoints ,String lat,String lon,String readingUnlistedPest,String readingimage,String pestReadingSprayBlockID,String customerID,String active ,String pestMonitoringPointName) {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues contentValues1 = new ContentValues();
     // contentValues1.put(COL1 ,mB4000PestMonitoringReadingsID);
      contentValues1.put(COL2, pestMonitoringReadingDate);
      contentValues1.put(COL3 ,pestMonitoringPointID);
      contentValues1.put(COL4 ,reading);
      contentValues1.put(COL5, monitoringPoints);
      contentValues1.put(COL7,lat);
      contentValues1.put(COL8,lon);
      contentValues1.put(COL10,readingUnlistedPest);
      contentValues1.put(COL11, readingimage);
      contentValues1.put("PestReadingSprayBlockID",pestReadingSprayBlockID);
      contentValues1.put(COL13, customerID);
      contentValues1.put(COL14,active);
      


      long result = db.insert(TABLE_NAME1,null, contentValues1);
      if(result == -1)
        return false;
      else
        return true;
    }



    public boolean updateNameStatus(int status ) {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues contentValues = new ContentValues();
      contentValues.put(COL8, status);
      db.update(TABLE_NAME, contentValues,status +" = 0",null);
      db.close();
      return true;
    }

    /*
     * this method will give us all the name stored in sqlite
     * */
    public Cursor getNames() {
      SQLiteDatabase db = this.getReadableDatabase();
      String sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + COL2+ " ASC;";
      Cursor cursor = db.rawQuery(sql, null);
      return cursor;
    }

    /*
     * this method is for getting all the unsynced name
     * so that we can sync it with database
     * */
    public Cursor getUnsyncedNames() {
      SQLiteDatabase db = this.getWritableDatabase();
      Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COL8 + " = 0;", null);
      return cursor;
    }



  }


