package co.za.dfmtechnologies.pestmonitoring.realm;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Logger
 */
public class Logger {

    public static void d(String tag, String msg) {
        Log.d(tag, msg);
    }

    public static void w(String tag, String msg) {
        Log.w(tag, msg);
    }

    public static void e(String tag, String msg) {
        Log.e(tag, msg);
    }

    public static void e(String tag, String msg, Throwable tr) {
        Log.e(tag, msg, tr);
    }

    public static void logErrorRemotely(Throwable tr) {
        if( Fabric.isInitialized()) {
            Crashlytics.logException(tr);
        }
    }
}
