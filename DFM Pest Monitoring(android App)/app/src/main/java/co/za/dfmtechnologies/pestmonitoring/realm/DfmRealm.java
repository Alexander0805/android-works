package co.za.dfmtechnologies.pestmonitoring.realm;

import android.content.Context;
import android.support.annotation.NonNull;



import co.za.dfmtechnologies.pestmonitoring.realm.model.User;
import io.realm.Realm;


/**
 * DFM Realm
 */
public class DfmRealm {

    private static DfmRealm INSTANCE;

    public static DfmRealm getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new DfmRealm();
        }

        return INSTANCE;
    }

    private DfmRealm() {
    }

    public void initRealm(@NonNull Context context) {
        Realm.init(context);
    }

    public void addOrUpdateUser(@NonNull User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.insertOrUpdate(user);
        realm.commitTransaction();
    }

    public User getCurrentUser() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(User.class).findFirst();
    }



    public void clearAllData() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }


}
