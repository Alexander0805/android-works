package co.za.dfmtechnologies.pestmonitoring;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;



import java.util.ArrayList;

import co.za.dfmtechnologies.R;


public class Blocks extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {
  private static final String TAG ="0" ;
  MyRecyclerViewAdapter adapter;
  String bn,met;
  String spid;
  Intent intent;
  ImageView setting,home;
  SharedPreferences pref;
  SprayblocksHelper sDb;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.blocks);
    sDb = new SprayblocksHelper(this);


    setting=(ImageView)findViewById(R.id.imageView6);
    setting.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
         Intent intent = new Intent(Blocks.this, Setting.class);
        Blocks.this.startActivity(intent);
      }
    });
    home =(ImageView) findViewById(R.id.imageView5);
    home.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(Blocks.this, MainActivity.class);
        Blocks.this.startActivity(intent);
      }
    });


    this.pref = getSharedPreferences("user_details", MODE_PRIVATE);
    //  this.intent = new Intent(Blocks.this, scouting.class);
    met = this.pref.getString("method", null);
    RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
    recyclerView.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager((Context)this));
    MyRecyclerViewAdapter myRecyclerViewAdapter = new MyRecyclerViewAdapter((Context)this, getAllBlocks());
    this.adapter = myRecyclerViewAdapter;
    myRecyclerViewAdapter.setClickListener(this);
    recyclerView.setAdapter(this.adapter);
    recyclerView.addItemDecoration((RecyclerView.ItemDecoration)new DividerItemDecoration(recyclerView.getContext(), 1));


    getSpid();

  }

  public void onItemClick(View paramView, int paramInt) {
    SharedPreferences.Editor editor = this.pref.edit();
    bn =  this.adapter.getItem(paramInt).trim();
    editor.putString("bn", this.adapter.getItem(paramInt)).apply();
    // Log.e(TAG,this.adapter.getItem(paramInt));
    // Log.d("sprayblockid",getSpid().get(0));
    editor.putString("spid", getSpid().get(0)).apply();

    editor.commit();
    startActivity(new Intent(Blocks.this,blockView.class));
  }

  public ArrayList<String> getAllBlocks() {


    ArrayList<String> Block = new ArrayList<>();
    try {
      SQLiteDatabase db = sDb.getWritableDatabase();
      // SQLiteDatabase Mdb = mdb.getWritableDatabase();
      //String sql = "SELECT Number FROM SprayBlocks";
      //String sql = "Select distinct sb.MB4000SprayBlockID,sb.BlockID,sb.Number --returns only single record because we are joining 2 tables\n" +
      //        "from "+ sDb.TABLE_NAME1 +" as PMP Inner Join "+ sDb.TABLE_NAME +" as SB on pmp.SprayBlockID = sb.MB4000SprayBlockID --Joining the 2 tables that we selecting data from (pmp = PestMonitoringPoints, sb=SprayBlocks)\n" +
      //        "order by sb.BlockID --Ordering by the block id in the spray blocks table\n";

      String sql ="SELECT O.Number,O.MB4000SprayBlockID , P.Pest,P.LoggingMethod FROM [Sprayblocks] O JOIN MonitoringPoints I ON O.MB4000SprayBlockID = I.SprayBlockID JOIN Pest P ON P.Pest = I.Pest   where P.LoggingMethod ="+met+" ORDER BY O.Number";


      Cursor cursor = db.rawQuery(sql, null);

      while (cursor.moveToNext()) {
        if (Block.contains(cursor.getString(cursor.getColumnIndex("Number")))) {
        } else {
          Block.add(cursor.getString(cursor.getColumnIndex("Number")));
        }
      }cursor.close();
    }catch(Exception ex){
      Log.e(TAG,"Error in getting Blocks "+ex.toString());
    }
    return Block;
  }



  public ArrayList<String> getSpid() {


    ArrayList<String> Sprayid = new ArrayList<>();
    try {
      SQLiteDatabase db = sDb.getWritableDatabase();
      String sql = "Select MB4000SprayBlockID from Sprayblocks where Number ='" + bn +"'";
      Cursor cursor = db.rawQuery(sql, null);

      while (cursor.moveToNext()) {
        Sprayid.add(cursor.getString(cursor.getColumnIndex("MB4000SprayBlockID")));
      }cursor.close();
    }catch(Exception ex){
      Log.e(TAG,"Error in getting Blocks "+ex.toString());
    }
    return Sprayid;
  }
  @Override
  public void startActivityForResult(Intent intent, int requestCode) {
    if (intent == null) {
      intent = new Intent();
    }
    super.startActivityForResult(intent, requestCode);
  }
}


