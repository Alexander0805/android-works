package co.za.dfmtechnologies.pestmonitoring.ui;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


/**
 * Base Presenter
 */
public interface BasePresenter<V extends BaseView> {

    void attach(@NonNull V view);

    void detach();

    @Nullable
    V getView();
}
