package co.za.dfmtechnologies.pestmonitoring;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.HttpGet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.za.dfmtechnologies.pestmonitoring.realm.DfmRealm;
import co.za.dfmtechnologies.pestmonitoring.rest.DfmEndpoint;
import co.za.dfmtechnologies.pestmonitoring.scouting;
import co.za.dfmtechnologies.R;
import co.za.dfmtechnologies.pestmonitoring.ui.login.LoginActivity;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class load extends AppCompatActivity {
  String al;
  ProgressBar mProgressBar;
  TextView greeting;
  int i = 0;
  Intent intent;
  CountDownTimer mCountDownTimer;
  private List list;
  private List list4;
  private List pest_list ;
  SprayblocksHelper sDb;

  String results;
  String sprayURL,pestURL,monURL;
  String CustID;
  String fVal;
  String hVal;
  String pas;
  SharedPreferences prf;


    String email;
    String cname;
  private int progressStatus = 0;



  private Handler handler = new Handler();

    @Override
  protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      setContentView(R.layout.load);
        prf = getSharedPreferences("user_details", MODE_PRIVATE);
        intent = new Intent(load.this, activity_pest_monitoring.class);
        al = prf.getString("usn", null);
        pas = prf.getString("pass", null);


        DfmRealm dfmRealm = DfmRealm.getInstance();
      email=  dfmRealm.getCurrentUser().getUsername();
      String cnme []= email.split("@");
cname= cnme[0];
      String log= "http://probe.dfmsoftware.co.za/WebServices/WSCustomers.ashx?Action=AUTHMB4000&Username="+al+"&UserPassword="+pas;
        new load.LongOperation1().execute(log);
      sDb = new SprayblocksHelper(this);

      list = new ArrayList<Integer>();
      pest_list = new ArrayList<String>();
      list4 = new ArrayList<String>();




      greeting = findViewById(R.id.textView5);

      mProgressBar = findViewById(R.id.progressBar);
      mProgressBar.setProgress(this.i);
      TextView textView = this.greeting;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Welcome  ");
        stringBuilder.append(this.cname);
      textView.setText(stringBuilder.toString());



      new Thread(new Runnable() {
          public void run() {
              while (progressStatus < 100) {
                  progressStatus += 1;
                  // Update the progress bar and display the
                  //current value in the text view

                  handler.post(new Runnable() {
                      public void run() {
                          mProgressBar.setProgress(progressStatus);

                      }

                  });
                  try {
                      // Sleep for 200 milliseconds.
                      Thread.sleep(200);
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }
              }
          }
      }).start();


  }
    private class LongOperation1 extends AsyncTask<String, Void, Void> {

        private final HttpClient Client = new DefaultHttpClient();
        private String Content;
        private String Error = null;
        //private ProgressDialog Dialog = new ProgressDialog(load.this);


        protected void onPreExecute() {
            // NOTE: You can call UI Element here.

            //UI Element

            // Dialog.setMessage("Syncing...");
            // Dialog.show();


        }

        // Call after onPreExecute method
        protected Void doInBackground(String... urls) {
            try {

                // Call long running operations here (perform background computation)
                // NOTE: Don't call UI Element here.

                // Server url call by GET method
                HttpGet httpget = new HttpGet(urls[0]);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                Content = Client.execute(httpget, responseHandler);

            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            // NOTE: You can call UI Element here.

            // Close progress dialog
            //Dialog.dismiss();

            if (Error != null) {

                Toast.makeText(load.this,"Sync Failed Please Check Internet Connection",Toast.LENGTH_SHORT).show();

                Intent i = new Intent(load.this, LoginActivity.class);
// set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                // user.setText("Output : " + Error);

            } else {
                results = String.valueOf(Content);

                String[] row = results.split("\\r\\n|\\n|\\r");

                String[] rod = row[0].split("\\|");
                String[] rod1 = row[1].split("\\|");
                hVal=(rod1[1]);
                String Acess= (rod1[0]);

                Log.d("custid",hVal);
                Log.d("acess",Acess);
                if(Acess.equals("ERROR")){
                    Toast.makeText(load.this,"Invalid ",Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor editor = prf.edit();
                    editor.clear();
                    editor.commit();
                    removeSprayBlocks();
                    DfmRealm dfmRealm = DfmRealm.getInstance();
                    dfmRealm.clearAllData();
                    Intent i = new Intent(load.this, LoginActivity.class);
// set the new task and clear flags
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);

                }else {
                    CustID=hVal;
                        prf = getSharedPreferences("user_details", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prf.edit();
                        editor.putString("cust",CustID).apply();
                        editor.commit();

                    sprayURL ="http://probe.dfmsoftware.co.za/WebServices/WSCustomers.ashx?Action=GetMB4000SPRAYBLOCK&Username="+al+"&userPassword="+pas+"&CustomerID="+CustID;
                    pestURL ="http://probe.dfmsoftware.co.za/WebServices/WSCustomers.ashx?Action=GetMB4000PESTS&Username="+al+"&userPassword="+pas+"&CustomerID="+CustID;
                    monURL ="http://probe.dfmsoftware.co.za/WebServices/WSCustomers.ashx?Action=GetMB4000PestMonitoringPoints&Username="+al+"&userPassword="+pas+"&CustomerID="+CustID;


                    networkcheck();

                }

            }
        }

    }
    // Class with extends AsyncTask class
    private class LongOperation extends AsyncTask<String, Void, Void> {

        private final HttpClient Client = new DefaultHttpClient();
        private String Content;
        private String Error = null;
        //private ProgressDialog Dialog = new ProgressDialog(load.this);


        protected void onPreExecute() {
            // NOTE: You can call UI Element here.

            //UI Element

           // Dialog.setMessage("Syncing...");
           // Dialog.show();


        }

        // Call after onPreExecute method
        protected Void doInBackground(String... urls) {
            try {

                // Call long running operations here (perform background computation)
                // NOTE: Don't call UI Element here.

                // Server url call by GET method
                HttpGet httpget = new HttpGet(urls[0]);
                ResponseHandler<String> responseHandler = new BasicResponseHandler();
                Content = Client.execute(httpget, responseHandler);

            } catch (ClientProtocolException e) {
                Error = e.getMessage();
                cancel(true);
            } catch (IOException e) {
                Error = e.getMessage();
                cancel(true);
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            // NOTE: You can call UI Element here.

            // Close progress dialog
            //Dialog.dismiss();

            if (Error != null) {

                Toast.makeText(load.this,"Sync Failed Please Check Internet Connection",Toast.LENGTH_SHORT).show();

                Intent i = new Intent(load.this, LoginActivity.class);
// set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                // user.setText("Output : " + Error);

            } else {


        if(String.valueOf(Content).equals("No Record Found")){
            Toast.makeText(load.this,"Please upload data before using app",Toast.LENGTH_LONG).show();
            SharedPreferences.Editor editor = prf.edit();
            editor.clear();
            editor.commit();
            removeSprayBlocks();
            DfmRealm dfmRealm = DfmRealm.getInstance();
            dfmRealm.clearAllData();
            Intent i = new Intent(load.this, LoginActivity.class);
// set the new task and clear flags
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
         }else {
      results = String.valueOf(Content);

      String[] row = results.split("\\r\\n|\\n|\\r");

      String[] rod = row[0].split("\\|");

      fVal = (rod[0]);


      if (fVal.equals("MB4000SprayBlockID")) {
          syncSprayBlocks();
      } else if ((fVal.equals("Pest"))) {
          syncPest();
      } else if (fVal.equals("MB4000PestMonitoringPointID")) {
          syncMoni();
      }

  }
            }
        }

    }
    public boolean removeSprayBlocks() {
        SQLiteDatabase sdb = sDb.getWritableDatabase();

        sdb.delete(sDb.TABLE_NAME,null, null);
        sdb.delete(sDb.TABLE_NAME1,null, null);
        sdb.delete(sDb.TABLE_NAME2,null, null);
        sdb.close();



        return true;
    }

    private void networkcheck () {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            connected = true;
        } else
            connected = false;

        if (connected == true) {

            removeSprayBlocks();

            new load.LongOperation().execute(sprayURL);
            new load.LongOperation().execute(monURL);
            new load.LongOperation().execute(pestURL);

            sDb.close();
            Intent i = new Intent(load.this, activity_pest_monitoring.class);
// set the new task and clear flags

            startActivity(i);
        } else
            Toast.makeText(getApplicationContext(), "No internet"+ "\n"+
                    "Please Connect to Internet and Try again", Toast.LENGTH_LONG).show();
    }
    public boolean syncSprayBlocks() {

        String[] row = results.split("\\r\\n|\\n|\\r");

        for (int i = 1; i < row.length; i++) {

            String[] rod = row[i].split("\\|");
            for (int j = 0; j < rod.length; j++) {
                pest_list.add(rod[j]);

            }
            if (pest_list.size() >1) {
                sDb.insertData(rod[0], rod[1], rod[2], rod[3], rod[4], rod[5], rod[6], rod[7], rod[8], rod[9], rod[10], rod[11], rod[12], rod[13], rod[14], rod[15], rod[16], rod[17], rod[18], rod[19], rod[20], rod[21], rod[22]);

                // Toast.makeText(MainActivity.this,""+pest_list.get(i-1),Toast.LENGTH_SHORT).show();
            }

        }
sDb.close();
        return true;
    }

    public boolean syncPest() {

        String[] row = results.split("\\r\\n|\\n|\\r");

        for (int i = 1; i < row.length; i++) {

            String[] rod = row[i].split("\\|");
            for (int j = 0; j < rod.length; j++) {
                list.add(rod[j]);

            }
            if (list.size() > 1) {

                sDb.insertPData(rod[0], rod[1], rod[2], rod[3], rod[4], rod[5], rod[6], rod[7], rod[8], rod[9], rod[10], rod[11]);
                // Toast.makeText(MainActivity.this,""+pest_list.get(i-1),Toast.LENGTH_SHORT).show();
            }
        }

        sDb.close();
        return true;
    }


    public boolean syncMoni() {

        String[] row = results.split("\\r\\n|\\n|\\r");

        for (int i = 1; i < row.length; i++) {

            String[] rod = row[i].split("\\|");
            for (int j = 0; j < rod.length; j++) {
                list4.add(rod[j]);

            }
            if (list4.size() > 1) {
                sDb.insertMdata(rod[0], rod[1], rod[2], rod[3], rod[4], rod[5], rod[6], rod[7], rod[8]);
                // Toast.makeText(MainActivity.this,""+pest_list.get(i-1),Toast.LENGTH_SHORT).show();
            }
        }
        sDb.close();
        //Toast.makeText(load.this, "data has been synced", Toast.LENGTH_SHORT).show();
        progressStatus=100;
        return true;
    }
}



