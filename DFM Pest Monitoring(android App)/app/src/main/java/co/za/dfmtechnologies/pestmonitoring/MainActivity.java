package co.za.dfmtechnologies.pestmonitoring;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import co.za.dfmtechnologies.R;
import co.za.dfmtechnologies.pestmonitoring.ui.BaseActivity;
import co.za.dfmtechnologies.pestmonitoring.realm.DfmRealm;
import co.za.dfmtechnologies.pestmonitoring.realm.model.User;
import co.za.dfmtechnologies.pestmonitoring.ui.login.LoginActivity;

import static co.za.dfmtechnologies.pestmonitoring.NotificationUtils.ANDROID_CHANNEL_ID;


public class MainActivity extends AppCompatActivity  implements NetworkStateReceiver.NetworkStateReceiverListener {
  Button cpt;
  Intent intent;
  Button sc,setting;
  SharedPreferences pref;
  String method;
  final Handler handler = new Handler();
  private NotificationUtils mNotificationUtils;
  DatabaseHelper myDb;
  NotificationCompat.Builder builder;
  NotificationManagerCompat notificationManager;
  int notificationId = 101;

  private NetworkStateReceiver networkStateReceiver;
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    setContentView(R.layout.method);

    startNetworkBroadcastReceiver(MainActivity.this);
    mNotificationUtils = new NotificationUtils(this);
    notificationManager = NotificationManagerCompat.from(this);
    builder = new NotificationCompat.Builder(this, ANDROID_CHANNEL_ID);

    myDb = new DatabaseHelper((Context) this);

    DfmRealm dfmRealm = DfmRealm.getInstance();
    User currentUser = dfmRealm.getCurrentUser();
    if(currentUser == null) {
      Intent loginIntent = new Intent(this, LoginActivity.class);
      this.startActivity(loginIntent);
      this.finish();
    }



    cpt = findViewById(R.id.button2);
    sc = findViewById(R.id.button3);

    this.pref = getSharedPreferences("user_details", MODE_PRIVATE);
    //   this.intent = new Intent(activity_pest_monitoring.this, Blocks.class);

    setting=findViewById(R.id.button4);
    setting.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, Setting.class);
        MainActivity.this.startActivity(intent);
      }
    });


    cpt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        method ="0";
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("method",method).apply();
        editor.commit();
        Intent intent = new Intent(MainActivity.this, Blocks.class);
        MainActivity.this.startActivity(intent);

      }
    });

    sc.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        method ="1";
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("method",method).apply();
        editor.commit();
        Intent intent = new Intent(MainActivity.this, Blocks.class);
        MainActivity.this.startActivity(intent);
      }
    });

  }
  public JSONArray getResults() {

    SQLiteDatabase myDataBase = myDb.getReadableDatabase();


    String searchQuery = "SELECT * FROM PestReading ";
    Cursor cursor = myDataBase.rawQuery(searchQuery, null);


    JSONArray resultSet = new JSONArray();
    JSONObject returnObj = new JSONObject();

    cursor.moveToFirst();
    while (cursor.isAfterLast() == false) {

      int totalColumn = cursor.getColumnCount();
      JSONObject rowObject = new JSONObject();

      for (int i = 0; i < totalColumn; i++) {
        if (cursor.getColumnName(i) != null) {

          try {

            if (cursor.getString(i) != null) {
              Log.d("TAG_NAME", cursor.getString(i));
              rowObject.put(cursor.getColumnName(i), cursor.getString(i));
            } else {
              rowObject.put(cursor.getColumnName(i), "");
            }
          } catch (Exception e) {
            Log.d("TAG_NAME", e.getMessage());
          }
        }

      }

      resultSet.put(rowObject);
      cursor.moveToNext();
    }

    cursor.close();
    myDb.close();
    Log.d("TAG_NAME", resultSet.toString());
    myDataBase.close();
    return resultSet;

  }

  public JSONArray getResults1() {

    SQLiteDatabase myDataBase = myDb.getReadableDatabase();


    String searchQuery = "SELECT * FROM PestReading1 ";
    Cursor cursor = myDataBase.rawQuery(searchQuery, null);


    JSONArray resultSet = new JSONArray();
    JSONObject returnObj = new JSONObject();

    cursor.moveToFirst();
    while (cursor.isAfterLast() == false) {

      int totalColumn = cursor.getColumnCount();
      JSONObject rowObject = new JSONObject();

      for (int i = 0; i < totalColumn; i++) {
        if (cursor.getColumnName(i) != null) {

          try {

            if (cursor.getString(i) != null) {
              Log.d("TAG_NAME", cursor.getString(i));
              rowObject.put(cursor.getColumnName(i), cursor.getString(i));
            } else {
              rowObject.put(cursor.getColumnName(i), "");
            }
          } catch (Exception e) {
            Log.d("TAG_NAME", e.getMessage());
          }
        }

      }

      resultSet.put(rowObject);
      cursor.moveToNext();
    }

    cursor.close();
    myDb.close();
    Log.d("TAG_NAME", resultSet.toString());
    myDataBase.close();
    return resultSet;

  }





      public class RequestAsync1 extends AsyncTask<String, String, String> { //sending unlisted pest


        @Override
        protected String doInBackground(String... strings) {

          try {


            int PROGRESS_MAX = getResults1().length();
            int PROGRESS_CURRENT = 0;
            builder.setContentTitle("Pest Monitoring")
                    .setContentText("Upload in progress")
                    .setSmallIcon(R.drawable.dfmreview)
                    .setOnlyAlertOnce(true)
                    .setOngoing(true)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                     .setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
            notificationManager.notify(notificationId, builder.build());

            JSONObject object = new JSONObject();

            for (int i = 0; i < getResults1().length(); i++) {

              int PROGRESS_CURRENT1 = i;

              builder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT1, false);
              notificationManager.notify(notificationId, builder.build());

              object = getResults1().getJSONObject(i);


              RequestHandler.sendPost1("http://probe.dfmsoftware.co.za/WebServices/MB4000PestMonitoringReadingsAndroid.ashx?Action=SaveMB4000PestMonitoringReadingsAndroid&", object);

            }

            updateNameStatus();


            return object.toString();

          } catch (Exception e) {


            return new String("Exception: " + e.getMessage());

          }

        }


        @Override
        protected void onPostExecute(String s) {



            builder.setContentText("Upload complete")
                    .setProgress(0, 0, false)
                    .setOngoing(false);
            notificationManager.notify(notificationId, builder.build());
            //     String title = "Successful";
            //     String author = "Readings uploaded";
//
            //     if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
            //         NotificationCompat.Builder nb = mNotificationUtils.
            //                 getAndroidChannelNotification(title, author);
//
            //         mNotificationUtils.getManager().notify(101, nb.build());
            // }
//

        }

        public boolean updateNameStatus() {
          SQLiteDatabase db = myDb.getWritableDatabase();

          db.delete(myDb.TABLE_NAME1, null, null);
          db.close();
          return true;
        }

      }



  public class RequestAsync extends AsyncTask<String, String, String> {


    @Override
    protected String doInBackground(String... strings) {


      try {

        JSONObject object = new JSONObject();

        for (int i = 0; i < getResults().length(); i++) {

          object = getResults().getJSONObject(i) ;


          RequestHandler.sendPost1("http://probe.dfmsoftware.co.za/WebServices/MB4000PestMonitoringReadingsAndroid.ashx?Action=SaveMB4000PestMonitoringReadingsAndroid&",object);

        }

        updateNameStts();


        return object.toString();

      } catch (Exception e) {


        return new String("Exception: " + e.getMessage());

      }

    }

    @Override
    protected void onPostExecute(String s) {
      if (s.contains("MB4000PestMonitoringReadingsID")) {

        String title = "Successful";
        String author = "Readings uploaded";

        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
          NotificationCompat.Builder nb = mNotificationUtils.
                  getAndroidChannelNotification(title, author);

          mNotificationUtils.getManager().notify(101, nb.build());
        }

      }
    }
    public boolean updateNameStts() {
      SQLiteDatabase db = myDb.getWritableDatabase();

      db.delete(myDb.TABLE_NAME,null, null);
      db.close();
      return true;
    }

  }
  public boolean  networkcheck () {

    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

      if( getResults().length()>0) {
        new RequestAsync().execute();
      }

      if( getResults1().length()>0) {
        new RequestAsync1().execute();
      }

       return true;

    } else
      return  false;
  }

  @Override
  protected void onPause() {
    /***/
    unregisterNetworkBroadcastReceiver(this);
    super.onPause();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {

        startNetworkBroadcastReceiver(MainActivity.this);

        handler.postDelayed(this,5000 );
      }
    }, 5000);
  }

  @Override
  protected void onResume() {
    /***/
    registerNetworkBroadcastReceiver(this);
    super.onResume();

    if( getResults().length()>0) {
      new RequestAsync().execute();
    }

    if( getResults1().length()>0) {
      new  RequestAsync1().execute();
    }

  }

  @Override
  public void networkAvailable() {
    Log.i("TAG", "networkAvailable()");

    if( getResults().length()>0) {
      new RequestAsync().execute();
    }

    if( getResults1().length()>0) {
      new RequestAsync1().execute();
    }

    //Proceed with online actcions in activity (e.g. hide offline UI from user, start services, etc...)
  }

  @Override
  public void networkUnavailable() {
    Log.i("TAG", "networkUnavailable()");
    //Proceed with offline actions in activity (e.g. sInform user they are offline, stop services, etc...)
  }

  public void startNetworkBroadcastReceiver(Context currentContext) {
    networkStateReceiver = new NetworkStateReceiver();
    networkStateReceiver.addListener((NetworkStateReceiver.NetworkStateReceiverListener) currentContext);
    registerNetworkBroadcastReceiver(currentContext);
  }

  /**
   * Register the NetworkStateReceiver with your activity
   * @param currentContext
   */
  public void registerNetworkBroadcastReceiver(Context currentContext) {
    currentContext.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
  }

  /**
   Unregister the NetworkStateReceiver with your activity
   * @param currentContext
   */
  public void unregisterNetworkBroadcastReceiver(Context currentContext) {
    currentContext.unregisterReceiver(networkStateReceiver);
  }
}
