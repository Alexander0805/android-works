package co.za.dfmtechnologies.pestmonitoring.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import co.za.dfmtechnologies.pestmonitoring.realm.DfmRealm;
import co.za.dfmtechnologies.pestmonitoring.rest.DfmRetrofit;


/**
 * Base Fragment
 */
public abstract class BaseFragment<P extends BasePresenter<V>, V extends BaseView> extends Fragment {

    private DfmRealm dfmRealm;
    private DfmRetrofit dfmRetrofit;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.dfmRealm = DfmRealm.getInstance();
        this.dfmRetrofit = DfmRetrofit.getInstance();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        P presenter = getPresenter();
        if (presenter != null) {
            presenter.detach();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(this.getLayoutResId(), container, false);
        ButterKnife.bind(this, view);

        this.createPresenter();

        P presenter = this.getPresenter();
        if (presenter != null) {
            presenter.attach(this.getBaseView());
        }

        this.setupViews();

        return view;
    }

    public boolean setToolbarTitle(@NonNull TextView title) {
        return false;
    }

    protected abstract
    @LayoutRes
    int getLayoutResId();

    protected abstract V getBaseView();

    @Nullable
    protected abstract P getPresenter();

    protected abstract void setupViews();

    protected abstract void createPresenter();

    public DfmRealm getDfmRealm() {
        return dfmRealm;
    }

    public DfmRetrofit getDfmRetrofit() {
        return dfmRetrofit;
    }
}
