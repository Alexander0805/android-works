package co.za.dfmtechnologies.pestmonitoring;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import co.za.dfmtechnologies.R;


public class activity_pest_monitoring extends AppCompatActivity {
    Button cpt;
    Intent intent;
    Button sc,setting;
    SharedPreferences pref;
    String method;
    DatabaseHelper myDb;
    private NotificationUtils mNotificationUtils;
    final Handler handler = new Handler();

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.method);

        cpt = findViewById(R.id.button2);
        sc = findViewById(R.id.button3);


        if(AppCompatDelegate.getDefaultNightMode()==AppCompatDelegate.MODE_NIGHT_YES){

        }

        mNotificationUtils = new NotificationUtils(this);
      myDb= new DatabaseHelper(this);


//       handler.postDelayed(new Runnable() {
//           @Override
//           public void run() {
//               SendReadings();
//               handler.postDelayed(this, 5000);
//           }
//       }, 5000);


        this.pref = getSharedPreferences("user_details", MODE_PRIVATE);
        this.intent = new Intent(activity_pest_monitoring.this, Blocks.class);

        setting=findViewById(R.id.button4);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity_pest_monitoring.this, Setting.class);
// set the new task and clear flags
                startActivity(i);
            }
        });


        cpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method ="0";
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("method",method).apply();
                editor.commit();
                Intent i = new Intent(activity_pest_monitoring.this, Blocks.class);
// set the new task and clear flags
                startActivity(i);
            }
        });

        sc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method ="1";
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("method",method).apply();
                editor.commit();
                Intent i = new Intent(activity_pest_monitoring.this, Blocks.class);
// set the new task and clear flags

                startActivity(i);
            }
        });

    }
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
}


    public JSONArray getResults1() {

        SQLiteDatabase myDataBase = myDb.getReadableDatabase();


        String searchQuery = "SELECT * FROM PestReading1 ";
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);


        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }

            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        myDataBase.close();
        Log.d("TAG_NAME", resultSet.toString());

        return resultSet;

    }


    public class RequestAsync1 extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {


            try {

                JSONObject object = new JSONObject();

                for (int i = 0; i < getResults1().length(); i++) {

                    object = getResults1().getJSONObject(i) ;


                    RequestHandler.sendPost1("http://probe.dfmsoftware.co.za/WebServices/MB4000PestMonitoringReadingsAndroid.ashx?Action=SaveMB4000PestMonitoringReadingsAndroid&",object);

                }

                updateNameStatus();


                return object.toString();

            } catch (Exception e) {


                return new String("Exception: " + e.getMessage());

            }

        }
        @Override
        protected void onPostExecute(String s) {
            if (s.contains("MB4000PestMonitoringReadingsID")) {

                String title = "Successful";
                String author = "Readings uploaded";

                if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
                    NotificationCompat.Builder nb = mNotificationUtils.
                            getAndroidChannelNotification(title, author);

                    mNotificationUtils.getManager().notify(101, nb.build());
                }

            }
        }
        public boolean updateNameStatus() {
            SQLiteDatabase db = myDb.getWritableDatabase();

            db.delete(myDb.TABLE_NAME1,null, null);
            db.close();
            return true;
        }

    }


    public JSONArray getResults() {

        SQLiteDatabase myDataBase = myDb.getReadableDatabase();


        String searchQuery = "SELECT * FROM PestReading ";
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);


        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }

            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        Log.d("TAG_NAME", resultSet.toString());
        myDataBase.close();
        return resultSet;

    }
    public class RequestAsync extends AsyncTask<String, String, String> {



        @Override
        protected String doInBackground(String... strings) {


            try {

                JSONObject object = new JSONObject();

                for (int i = 0; i < getResults().length(); i++) {

                    object = getResults().getJSONObject(i) ;

                    RequestHandler.sendPost1("http://probe.dfmsoftware.co.za/WebServices/MB4000PestMonitoringReadingsAndroid.ashx?Action=SaveMB4000PestMonitoringReadingsAndroid&",object);

                }

                updateNameStatus();

                return object.toString();

            } catch (Exception e) {


                return new String("Exception: " + e.getMessage());

            }

        }
        @Override
        protected void onPostExecute(String s) {
            if (s.contains("MB4000PestMonitoringReadingsID")) {

                String title = "Successful";
                String author = "Readings uploaded";

                if(!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
                    NotificationCompat.Builder nb = mNotificationUtils.
                            getAndroidChannelNotification(title,  author);

                    mNotificationUtils.getManager().notify(101, nb.build());
                }

            }
        }

        public boolean updateNameStatus() {
            SQLiteDatabase db = myDb.getWritableDatabase();

            db.delete(myDb.TABLE_NAME,null, null);
            db.close();
            return true;
        }

    }

    public void SendReadings () {
        boolean connected = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnected()) {

            connected = true;
        } else
            connected = false;

        if (connected == true) {
            new RequestAsync().execute();
            new RequestAsync1().execute();
        } else
            Toast.makeText(getApplicationContext(), "No internet", Toast.LENGTH_LONG).cancel();
    }


}

