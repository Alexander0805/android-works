package co.za.dfmtechnologies.pestmonitoring;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import co.za.dfmtechnologies.R;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.NotificationChannel.DEFAULT_CHANNEL_ID;
import static co.za.dfmtechnologies.pestmonitoring.NotificationUtils.ANDROID_CHANNEL_ID;

public class scouting extends AppCompatActivity implements AdapterView.OnItemClickListener, NetworkStateReceiver.NetworkStateReceiverListener {

    private NetworkStateReceiver networkStateReceiver;      // Receiver that detects network state changes


    public static final int NAME_NOT_SYNCED_WITH_SERVER = 0;
    private static final String TAG = "0";

    String DD;

    String al;

    TextView bn;

    Button btn;

    CheckBox check;

    CheckBox check2;

    Intent intent;

    String la;

    TextView mp;

    DatabaseHelper myDb;
    LocationManager locationManager;
    String latitude, longitude;
    String imageString;
    SharedPreferences pref;
    private List<Names> names;
    SharedPreferences prf;
    int gt;
    String spid;
    String readingP;
    ImageView setting, home;
    EditText ulpest;
    ImageView mPreviewIv;
    String ur, cameraPermission[];
    SprayblocksHelper sDb;
    final Handler handler = new Handler();
    String cust;
    ListView rkt_ListView;
    Button btn_show_me, add, btblock;
    private ArrayList<String> kaminey_dost_array_list = new ArrayList<String>();
    private ArrayList<String> Block = new ArrayList<>();
    private ArrayList<String> monipest = new ArrayList<String>();
    private ArrayList<String> MPid = new ArrayList<String>();
    private ArrayList<String> MPN = new ArrayList<String>();
    private static final int REQUEST_LOCATION = 1, CAMERA_REQUEST_CODE = 200, IMAGE_PICK_CAMERA_CODE = 1001;
    RktArrayAdapter rktArrayAdapter;
    Uri image_uri;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    private NotificationUtils mNotificationUtils;
    NotificationCompat.Builder builder;
    NotificationManagerCompat notificationManager;
    int notificationId = 101;
LocationRequest locationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.scouting);

        startNetworkBroadcastReceiver(this);

        mNotificationUtils = new NotificationUtils(this);
        names = new ArrayList<>();
        notificationManager = NotificationManagerCompat.from(scouting.this);
        builder = new NotificationCompat.Builder(scouting.this, ANDROID_CHANNEL_ID);


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER )||!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            showAlert();

        } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER )||!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            setupLocationService();
            getLocation();

        }
        rkt_ListView = (ListView) findViewById(R.id.rkt_listview);


        //Log.d("comma",toCSV(GetStringArray(kaminey_dost_array_list)));
setupLocationService();
        getLocation();

        myDb = new DatabaseHelper((Context) this);
        sDb = new SprayblocksHelper(this);
        gt = 0;
        mp = findViewById(R.id.textView9);
        bn = findViewById(R.id.textView8);
        btn = (Button) findViewById(R.id.btn_show_me);
        prf = getSharedPreferences("user_details", MODE_PRIVATE);
        pref = getSharedPreferences("Login", MODE_PRIVATE);
        intent = new Intent(scouting.this, blockView.class);
        al = prf.getString("mp", null);
        la = prf.getString("bn", null);
        ur = pref.getString("user", null);
        cust = prf.getString("cust", null);
        mp.setText("Monitoring Point: " + al);
        bn.setText("Block: " + la);
        spid = prf.getString("spid", null);
        cameraPermission = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        mPreviewIv = findViewById(R.id.imageView2);
        ulpest = findViewById(R.id.editText);
        home = (ImageView) findViewById(R.id.imageView5);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                locationManager.removeUpdates(locationListenerGPS);
                locationManager.removeUpdates(locationListenerNetwork);
                Intent intent = new Intent(scouting.this, activity_pest_monitoring.class);
                scouting.this.startActivity(intent);
            }
        });
        btblock = findViewById(R.id.button8);
        btblock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                locationManager.removeUpdates(locationListenerGPS);
                locationManager.removeUpdates(locationListenerNetwork);
                Intent intent = new Intent(scouting.this, Blocks.class);
                scouting.this.startActivity(intent);


            }
        });


        add = findViewById(R.id.button4);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Location", longitude + latitude);

                if (longitude.equals("null") || longitude.equals("null")) {
                    Toast.makeText(scouting.this, "Location not ready please wait...", Toast.LENGTH_SHORT).show();
                } else {
                    getLocation();
                    String pest = ulpest.getText().toString();


                    if (pest.equals("")) {
                        Toast.makeText(scouting.this, "Please add unlisted  Pest submitting", Toast.LENGTH_SHORT).show();
                    } else {

                        if (!checkCameraPermission()) {
                            //camera permission not allowed ,request it
                            requestCameraPermission();
                        } else {
                            //permission allowed, take picture
                            getLocation();
                            pickCamera();


                        }

                    }
                }
            }
        });
        setting = (ImageView) findViewById(R.id.imageView6);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationManager.removeUpdates(locationListenerGPS);
                locationManager.removeUpdates(locationListenerNetwork);
                Intent intent = new Intent(scouting.this, Setting.class);
                scouting.this.startActivity(intent);
            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject object = new JSONObject();
                    // do some stuff....
                    for (int n = 0; n < getResults().length(); n++) {

                        object = getResults().getJSONObject(n);

                        Toast.makeText((Context) scouting.this, "" + object, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {


                    return;
                }
            }
        });


        //   handler.postDelayed(new Runnable() {
        //       @Override
        //       public void run() {
        //           if(networkcheck()==true) {

        //               if( getResults().length()>0) {
        //                   new RequestAsync().execute();
        //               }

        //               if( getResults1().length()>0) {

////sue the initial notification with zero progress


        //                   new RequestAsync1().execute();

        //               }
        //           }
        //           handler.postDelayed(this,5000 );
        //       }
        //   }, 5000);


        rktArrayAdapter = new RktArrayAdapter(
                this,
                R.layout.list_row,
                android.R.id.text1,
                getpest()

        );

        rkt_ListView.setAdapter(rktArrayAdapter);
        rkt_ListView.setOnItemClickListener(this);

        btn_show_me = (Button) findViewById(R.id.btn_show_me);
        btn_show_me.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                if (longitude.equals("null") || longitude.equals("null")) {
                    Toast.makeText(scouting.this, "Location not ready please wait...", Toast.LENGTH_SHORT).show();
                } else {
                    getLocation();
                    String result = "";
                    String result1 = "";
                    List<String> resultList = rktArrayAdapter.getCheckedItems();
                    List<String> leftpest = getAllpest();

                    for (int i = 0; i < resultList.size(); i++) {
                        result += String.valueOf(resultList.get(i)) + "\n";

                        String str = DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                        myDb.insertData(str, getMonitoringpointsid(al, resultList.get(i)).get(0), "1", getMonitoringpointsNumber(al, resultList.get(i)).get(0), spid, cust, "1", latitude, longitude, resultList.get(i));
                        // myDb.insertData("3651" ,"8987","5", "2","2", "9999", "123456", "1");
                        //gt=1;


                        Iterator<String> itr = leftpest.iterator();
                        while (itr.hasNext()) {
                            String array_list = itr.next();
                            if (array_list.contains(resultList.get(i))) {
                                itr.remove();
                            }


                        }
                        MPid.clear();
                        MPN.clear();
                    }

                    //Toast.makeText((Context) scouting.this, "Reading Added", Toast.LENGTH_SHORT).show();

                    rktArrayAdapter.getCheckedItemPositions().toString();
                    // Toast.makeText((Context) scouting.this, "" + leftpest, Toast.LENGTH_SHORT).show();

                    if (resultList.size() < 1) {

                        buildAlertMessageNoPest(leftpest);

                    } else {
                        getLocation();
                        for (int i = 0; i < leftpest.size(); i++) {
                            result1 += String.valueOf(leftpest.get(i)) + "\n";

                            String str = DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                            myDb.insertData(str, getMonitoringpointsid(al, leftpest.get(i)).get(0), "0", getMonitoringpointsNumber(al, leftpest.get(i)).get(0), spid, cust, "1", latitude, longitude, leftpest.get(i));
                            // myDb.insertData("3651" ,"8987","5", "2","2", "9999", "123456", "1");
                            //gt=1;
                            MPid.clear();
                            MPN.clear();
                        }
                        Toast.makeText((Context) scouting.this, "Readings Added", Toast.LENGTH_SHORT).show();

                        getMonitoringpoints();
                        Log.d("list size", String.valueOf(Block.size()));
                        if (Block.size() > 1) {
                            locationManager.removeUpdates(locationListenerGPS);
                            locationManager.removeUpdates(locationListenerNetwork);
                            Intent intent = new Intent(scouting.this, blockView.class);
                            scouting.this.startActivity(intent);
                        } else {
                            locationManager.removeUpdates(locationListenerGPS);
                            locationManager.removeUpdates(locationListenerNetwork);
                            Intent intent = new Intent(scouting.this, Blocks.class);
                            scouting.this.startActivity(intent);
                        }

                    }
                }
            }
        });
    }
    private void setupLocationService() {
        Log.v(TAG, "Setting up location service");
         LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(1 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        builder.setAlwaysShow(true);

        task.addOnSuccessListener(scouting.this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Log.v(TAG, "Location settings satisfied");
            }
        });

        task.addOnFailureListener(scouting.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        Log.w(TAG, "Location settings not satisfied, attempting resolution intent");
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(scouting.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException sendIntentException) {
                            Log.e(TAG, "Unable to start resolution intent");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.w(TAG, "Location settings not satisfied and can't be changed");
                        break;
                }
            }
        });
    }
    private boolean checkLocation() {
        if (!isLocationEnabled()){
            showAlert();}else{
            setupLocationService();
        getLocation();}
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        setupLocationService();
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public ArrayList<String> getMonitoringpoints() {


        try {
            SQLiteDatabase db = sDb.getWritableDatabase();


            //String sql ="Select PestMonitoringPoint From MonitoringPoints where SprayBlockID='"+spid+"'";
            String sql = "SELECT O.Number,I.PestMonitoringPoint ,O.MB4000SprayBlockID , P.Pest,P.LoggingMethod FROM [Sprayblocks] O JOIN MonitoringPoints I ON O.MB4000SprayBlockID = I.SprayBlockID JOIN Pest P ON P.Pest = I.Pest   where P.LoggingMethod =1 and I.SprayBlockID='" + spid + "' ORDER BY I.PestMonitoringPoint";
            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                if (Block.contains(cursor.getString(cursor.getColumnIndex("PestMonitoringPoint")))) {
                } else {
                    Block.add(cursor.getString(cursor.getColumnIndex("PestMonitoringPoint")));
                }

            }
            cursor.close();
            sDb.close();
        } catch (Exception ex) {
            Log.e(TAG, "Error in getting points " + ex.toString());
        }

        return Block;
    }


    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(scouting.this, ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (scouting.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(scouting.this, new String[]{ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 50, 0, locationListenerNetwork);
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 5000, 5, locationListenerGPS);

            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (location != null) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 50, 0, locationListenerNetwork);
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                latitude = String.valueOf(latti);
                longitude = String.valueOf(longi);


            } else if (location1 != null) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 50, 0, locationListenerGPS);
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                latitude = String.valueOf(latti);
                longitude = String.valueOf(longi);


            } else if (location2 != null) {
                locationManager.requestLocationUpdates(
                        LocationManager.PASSIVE_PROVIDER, 50, 0, locationListenerGPS);
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                latitude = String.valueOf(latti);
                longitude = String.valueOf(longi);


            }
        }
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
    }


    private void buildAlertMessageNoPest(final List<String> list) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure there is no pest?")
                .setTitle("No Pest Selected")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getLocation();
                        String result1 = "";
                        for (int a = 0; a < list.size(); a++) {
                            result1 += String.valueOf(list.get(a)) + "\n";

                            String str = DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                            myDb.insertData(str, getMonitoringpointsid(al, list.get(a)).get(0), "0", getMonitoringpointsNumber(al, list.get(a)).get(0), spid, cust, "1", latitude, longitude, list.get(a));
                            // myDb.insertData("3651" ,"8987","5", "2","2", "9999", "123456", "1");
                            //gt=1;
                            MPid.clear();
                            MPN.clear();
                        }

                        Toast.makeText((Context) scouting.this, "Readings Added", Toast.LENGTH_SHORT).show();
                        getMonitoringpoints();
                        Log.d("list size", String.valueOf(Block.size()));
                        if (Block.size() > 1) {
                            Intent intent = new Intent(scouting.this, blockView.class);
                            scouting.this.startActivity(intent);
                        } else {
                            Intent intent = new Intent(scouting.this, Blocks.class);
                            scouting.this.startActivity(intent);
                        }

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(
                                getApplicationContext(),
                                "Please select Pest ",
                                Toast.LENGTH_LONG).show();
                        dialogInterface.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        rktArrayAdapter.rkt_toggleChecked(i);

    }

    public class RktArrayAdapter extends ArrayAdapter<String> {

        private HashMap<Integer, Boolean> myChecked = new HashMap<Integer, Boolean>();

        public RktArrayAdapter(Context context, int resource,
                               int textViewResourceId, List<String> objects) {
            super(context, resource, textViewResourceId, objects);

            for (int i = 0; i < objects.size(); i++) {
                myChecked.put(i, false);
            }
        }

        public void rkt_toggleChecked(int position) {
            if (myChecked.get(position)) {
                myChecked.put(position, false);
            } else {
                myChecked.put(position, true);
            }

            notifyDataSetChanged();
        }

        public List<Integer> getCheckedItemPositions() {
            List<Integer> checkedItemPositions = new ArrayList<Integer>();

            for (int i = 0; i < myChecked.size(); i++) {
                if (myChecked.get(i)) {
                    (checkedItemPositions).add(i);
                }
            }

            return checkedItemPositions;
        }

        public List<String> getCheckedItems() {
            List<String> checkedItems = new ArrayList<String>();

            for (int i = 0; i < myChecked.size(); i++) {
                if (myChecked.get(i)) {
                    (checkedItems).add(getAllpest().get(i));
                }
            }

            return checkedItems;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            if (row == null) {
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(R.layout.list_row, parent, false);
            }

            CheckedTextView checked_TextView = (CheckedTextView) row.findViewById(R.id.checked_textview);
            checked_TextView.setText(getAllpest().get(position));

            Boolean checked = myChecked.get(position);
            if (checked != null) {
                checked_TextView.setChecked(checked);
            }

            return row;
        }

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(scouting.this,
                    blockView.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("EXIT", true);
            startActivity(intent);
        }
        return super.onKeyDown(keyCode, event);
    }

    public ArrayList<String> getAllpest() {


        ArrayList<String> kaminey_dost_array_list = new ArrayList<>();

        try {
            SQLiteDatabase db = sDb.getWritableDatabase();
            //String sql = "SELECT Pest FROM MonitoringPoints where PestMonitoringPoint=" +al;
            String sql = "SELECT O.Number,I.PestMonitoringPoint,O.MB4000SprayBlockID ,\n" +
                    "       P.Pest,P.LoggingMethod,I.MB4000PestMonitoringPointID\n" +
                    "  FROM [Sprayblocks] O \n" +
                    "  JOIN MonitoringPoints I ON O.MB4000SprayBlockID = I.SprayBlockID \n" +
                    "  JOIN Pest P ON P.Pest = I.Pest   where P.LoggingMethod = 1 and I.PestMonitoringPoint='" + al + "' and O.Number='" + la + "'";

            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                kaminey_dost_array_list.add(cursor.getString(cursor.getColumnIndex("Pest")));
            }
            cursor.close();
            sDb.close();
        } catch (Exception ex) {
            Log.e("TAG", "Error in getting Pest " + ex.toString());
        }

        return kaminey_dost_array_list;
    }


    public ArrayList<String> getpest() {
        try {
            SQLiteDatabase db = sDb.getWritableDatabase();
            // String sql = "Select distinct sb.Pest,sb.LoggingMethod\n" +
            //       " from MonitoringPoints  as PMP Inner Join Pest as SB on pmp.Pest= sb.Pest and sb.LoggingMethod=1 and pmp.PestMonitoringPoint='"+al+"'";
            String sql = "SELECT O.Number,I.PestMonitoringPoint,O.MB4000SprayBlockID ,\n" +
                    "       P.Pest,P.LoggingMethod,I.MB4000PestMonitoringPointID\n" +
                    "  FROM [Sprayblocks] O \n" +
                    "  JOIN MonitoringPoints I ON O.MB4000SprayBlockID = I.SprayBlockID \n" +
                    "  JOIN Pest P ON P.Pest = I.Pest   where P.LoggingMethod = 1 and I.PestMonitoringPoint='" + al + "' and O.Number='" + la + "'";

            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {

                monipest.add(cursor.getString(cursor.getColumnIndex("Pest")));

            }
            cursor.close();
            sDb.close();
        } catch (Exception ex) {
            Log.e("TAG", "Error in getting Pest " + ex.toString());
        }


        return monipest;
    }


    public JSONArray getResults1() {

        SQLiteDatabase myDataBase = myDb.getReadableDatabase();


        String searchQuery = "SELECT * FROM PestReading1 ";
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);


        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }

            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        myDataBase.close();
        Log.d("TAG_NAME", resultSet.toString());

        return resultSet;

    }


    public class RequestAsync1 extends AsyncTask<String, String, String> { //sending unlisted pest


        @Override
        protected String doInBackground(String... strings) {

            try {

                int PROGRESS_MAX = getResults1().length();
                int PROGRESS_CURRENT = 0;
                builder.setContentTitle("Pest Monitoring")
                        .setContentText("Upload in progress")
                        .setSmallIcon(R.drawable.dfmreview)
                        .setOnlyAlertOnce(true)
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_LOW)
                        .setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
                notificationManager.notify(notificationId, builder.build());

                JSONObject object = new JSONObject();

                for (int i = 0; i < getResults1().length(); i++) {

                    int PROGRESS_CURRENT1 = i;

                    builder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT1, false);
                    //notificationManager.notify(notificationId, builder.build());

                    object = getResults1().getJSONObject(i);


                    RequestHandler.sendPost1("http://probe.dfmsoftware.co.za/WebServices/MB4000PestMonitoringReadingsAndroid.ashx?Action=SaveMB4000PestMonitoringReadingsAndroid&", object);

                }

                updateNameStatus();


                return object.toString();

            } catch (Exception e) {


                return new String("Exception: " + e.getMessage());

            }

        }


        @Override
        protected void onPostExecute(String s) {

                builder.setContentText("Upload complete")
                        .setOngoing(false)
                        .setProgress(0, 0, false);
                notificationManager.notify(notificationId, builder.build());
                //     String title = "Successful";
                //     String author = "Readings uploaded";
//
                //     if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
                //         NotificationCompat.Builder nb = mNotificationUtils.
                //                 getAndroidChannelNotification(title, author);
//
                //         mNotificationUtils.getManager().notify(101, nb.build());
                // }
//

        }

        public boolean updateNameStatus() {
            SQLiteDatabase db = myDb.getWritableDatabase();

            db.delete(myDb.TABLE_NAME1, null, null);
            db.close();
            return true;
        }

    }


    public JSONArray getResults() {

        SQLiteDatabase myDataBase = myDb.getReadableDatabase();


        String searchQuery = "SELECT * FROM PestReading ";
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);


        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }

            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        Log.d("TAG_NAME", resultSet.toString());
        myDataBase.close();
        return resultSet;

    }

    public class RequestAsync extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {


            try {

                JSONObject object = new JSONObject();

                for (int i = 0; i < getResults().length(); i++) {

                    object = getResults().getJSONObject(i);

                    RequestHandler.sendPost1("http://probe.dfmsoftware.co.za/WebServices/MB4000PestMonitoringReadingsAndroid.ashx?Action=SaveMB4000PestMonitoringReadingsAndroid&", object);

                }

                updateNameStatus();

                return object.toString();

            } catch (Exception e) {


                return new String("Exception: " + e.getMessage());

            }

        }

        @Override
        protected void onPostExecute(String s) {
            if (s.contains("MB4000PestMonitoringReadingsID")) {

                String title = "Successful";
                String author = "Readings uploaded";

                if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
                    NotificationCompat.Builder nb = mNotificationUtils.
                            getAndroidChannelNotification(title, author);

                    mNotificationUtils.getManager().notify(101, nb.build());
                }

            }
        }

        public boolean updateNameStatus() {
            SQLiteDatabase db = myDb.getWritableDatabase();

            db.delete(myDb.TABLE_NAME, null, null);
            db.close();
            return true;
        }

    }

 public boolean  networkcheck () {

     ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
     if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
             connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

         if( getResults().length()>0) {
             new RequestAsync().execute();
         }

         if( getResults1().length()>0) {
             new RequestAsync1().execute();
         }

         return true;
     } else
         return  false;
 }



    public ArrayList<String> getMonitoringpointsid(String a, String p) {


        try {
            SQLiteDatabase db = sDb.getWritableDatabase();

            String sql = "SELECT O.MB4000PestMonitoringPointID,\n" +
                    "       I.Pest,I.LoggingMethod\n" +
                    "  FROM MonitoringPoints O \n" +
                    "  JOIN Pest I ON I.Pest= O.Pest \n" +
                    "where O.PestMonitoringPoint='" + a + "' and I.LoggingMethod=1 and O.pest='" + p + "'";

            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                MPid.add(cursor.getString(cursor.getColumnIndex("MB4000PestMonitoringPointID")));
            }
            cursor.close();
        } catch (Exception ex) {
            Log.e(TAG, "Error in getting points ID " + ex.toString());
        }

        return MPid;
    }

    public ArrayList<String> getMonitoringpointsNumber(String a, String p) {


        try {
            SQLiteDatabase db = sDb.getWritableDatabase();

            String sql = "SELECT O.MB4000PestMonitoringPointID,\n" +
                    "       I.Pest,I.LoggingMethod,\n" +
                    "       O.MonitoringPoints\n" +
                    "  FROM MonitoringPoints O \n" +
                    "  JOIN Pest I ON I.Pest= O.Pest \n" +
                    "where O.PestMonitoringPoint='" + a + "' and I.LoggingMethod=1 and O.pest= '" + p + "'";

            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                MPN.add(cursor.getString(cursor.getColumnIndex("MonitoringPoints")));
            }
            cursor.close();
            db.close();
        } catch (Exception ex) {
            Log.e(TAG, "Error in getting MPN" + ex.toString());
        }

        return MPN;
    }

    private void pickCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, ulpest.getText().toString());
        values.put(MediaStore.Images.Media.DESCRIPTION, "unLpest");
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);


    }


    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, cameraPermission, CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        boolean result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);

        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted && writeStorageAccepted) {
                        pickCamera();
                    } else {
                        Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

        }
    }

    //handle image

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //got image from camera

        //     if (requestCode == IMAGE_PICK_CAMERA_CODE) {
        //got image from gallery now crop
        //         CropImage.activity(image_uri)
        //                 .setGuidelines(CropImageView.Guidelines.ON)// enabled image guide lines
        //                 .start(this);
        //  }

        //get image cropped image
        //    if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
        //      CropImage.ActivityResult result = CropImage.getActivityResult(data);
        //      if (resultCode == RESULT_OK){
        //     Uri resultUri = result.getUri();//get image uri
        //set image to view


        mPreviewIv.setImageURI(image_uri);


        //get drawable bitmap for text recognition
        BitmapDrawable bitmapDrawable = (BitmapDrawable) mPreviewIv.getDrawable();
        Bitmap bitmap = bitmapDrawable.getBitmap();

       imageString = getStringImage(bitmap);
        //Log.d("base64",""+ imageString);

        String str = DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());



        myDb.insertUlistedPest(str, "", "1", "1", latitude, longitude, ulpest.getText().toString(), imageString, spid, cust, "1",la);
        ulpest.setText("");



        Toast.makeText((Context) scouting.this, "Unlisted Reading Added", Toast.LENGTH_SHORT).show();
        //Toast.makeText(scouting.this, "Network Provider update", Toast.LENGTH_SHORT).show();


        locationManager.removeUpdates(locationListenerGPS);
        locationManager.removeUpdates(locationListenerNetwork);


    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 30, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {

            double latti = location.getLatitude();
            double longi = location.getLongitude();
            latitude = String.valueOf(latti);
            longitude = String.valueOf(longi);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    //Toast.makeText(scouting.this, "Location Ready", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        getLocation();
        }

        @Override
        public void onProviderEnabled(String s) {
         getLocation();
        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };


    private final LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {

            location.getAccuracy();
            double latti = location.getLatitude();
            double longi = location.getLongitude();
            latitude = String.valueOf(latti);
            longitude = String.valueOf(longi);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //Toast.makeText(scouting.this, "Network Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
       getLocation();
        }

        @Override
        public void onProviderEnabled(String s) {
       getLocation();
        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (intent == null) {
            intent = new Intent();
        }
        super.startActivityForResult(intent, requestCode);
    }
    @Override
    protected void onPause() {
        /***/
        unregisterNetworkBroadcastReceiver(this);
        super.onPause();

    }

    @Override
    protected void onResume() {
        /***/
        registerNetworkBroadcastReceiver(this);

        super.onResume();

        if( getResults().length()>0) {
            new RequestAsync().execute();
        }

        if( getResults1().length()>0) {
            new RequestAsync1().execute();
        }


    }

    @Override
    public void networkAvailable() {
        Log.i(TAG, "networkAvailable()");

        if( getResults().length()>0) {
            new RequestAsync().execute();
        }

        if( getResults1().length()>0) {
            new RequestAsync1().execute();
        }

        //Proceed with online actions in activity (e.g. hide offline UI from user, start services, etc...)
    }

    @Override
    public void networkUnavailable() {
        Log.i(TAG, "networkUnavailable()");
        //Proceed with offline actions in activity (e.g. sInform user they are offline, stop services, etc...)
    }

    public void startNetworkBroadcastReceiver(Context currentContext) {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener((NetworkStateReceiver.NetworkStateReceiverListener) currentContext);
        registerNetworkBroadcastReceiver(currentContext);
    }

    /**
     * Register the NetworkStateReceiver with your activity
     * @param currentContext
     */
    public void registerNetworkBroadcastReceiver(Context currentContext) {
        currentContext.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     Unregister the NetworkStateReceiver with your activity
     * @param currentContext
     */
    public void unregisterNetworkBroadcastReceiver(Context currentContext) {
        currentContext.unregisterReceiver(networkStateReceiver);
    }
}
