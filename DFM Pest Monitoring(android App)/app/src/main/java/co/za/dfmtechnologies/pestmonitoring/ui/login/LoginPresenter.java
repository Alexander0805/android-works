package co.za.dfmtechnologies.pestmonitoring.ui.login;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;

import co.za.dfmtechnologies.pestmonitoring.rest.DfmRetrofit;
import co.za.dfmtechnologies.pestmonitoring.rest.GrantType;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Login Presenter
 */
public class LoginPresenter implements LoginContract.Presenter {

    private final DfmRetrofit dfmRetrofit;
    private LoginContract.View view;
    private CompositeDisposable compositeDisposable;

    public LoginPresenter(@NonNull DfmRetrofit dfmRetrofit) {
        this.dfmRetrofit = dfmRetrofit;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attach(@NonNull LoginContract.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        this.compositeDisposable.clear();
        this.view = null;
    }

    @Nullable
    @Override
    public LoginContract.View getView() {
        return this.view;
    }

    @SuppressLint("CheckResult")
    @Override
    public void onLoginButtonClicked() {

        String emailAddress = this.view.getEmailAddress();
        String password = this.view.getPassword();

        if(!this.validateEmailAddress(emailAddress) || !this.validatePassword(password)) {
            return;
        }

        this.view.showLoginProgress();
        this.dfmRetrofit.authenticateUser(GrantType.PASSWORD.getValue(), emailAddress, password)
                .doOnSubscribe(this.compositeDisposable::add)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> this.view.doOnLoginSuccess(), throwable -> this.view.showLoginError());
    }

    private boolean validateEmailAddress(@NonNull String emailAddress) {
        if(TextUtils.isEmpty(emailAddress) || !Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()) {
            this.view.showEmailAddressError();
            return false;
        }

        return true;
    }

    private boolean validatePassword(@NonNull String password) {

        if(TextUtils.isEmpty(password)) {
            this.view.showPasswordError();
            return false;
        }

        return true;
    }
}
