package co.za.dfmtechnologies.pestmonitoring.rest;

import co.za.dfmtechnologies.pestmonitoring.realm.model.User;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;



/**
 * DFM Endpoint
 */
public interface DfmEndpoint {

    @FormUrlEncoded
    @POST("authentication")
    Observable<User> authenticateUser(@Field("grant_type") String grantType,
                                      @Field("username") String userName,
                                      @Field("password") String password);


    @FormUrlEncoded
    @POST("authentication")
    Observable<User> refreshAuthorizationToken(@Field("grant_type") String grantType,
                                               @Field("refresh_token") String refreshToken);
}
