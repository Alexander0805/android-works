package co.za.dfmtechnologies.pestmonitoring.ui.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.za.dfmtechnologies.R;
import co.za.dfmtechnologies.pestmonitoring.load;
import co.za.dfmtechnologies.pestmonitoring.ui.BaseActivity;



/**
 * Login Activity
 */
public class LoginActivity extends BaseActivity<LoginContract.Presenter, LoginContract.View> implements LoginContract.View {

    @BindView(R.id.edit_email)
    EditText editEmail;

    @BindView(R.id.edit_password)
    EditText editPassword;

    @BindView(R.id.input_email)
    TextInputLayout inputEmail;

    @BindView(R.id.input_password)
    TextInputLayout inputPassword;

    private LoginContract.Presenter presenter;
    SharedPreferences pref;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected LoginContract.View getBaseView() {
        return this;
    }

    @Override
    protected void createPresenter() {
        this.presenter = new LoginPresenter(this.getDfmRetrofit());
    }

    @Nullable
    @Override
    protected LoginContract.Presenter getPresenter() {
        return this.presenter;
    }

    @Override
    protected void setupViews() {
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_login)
    public void onLoginButtonClicked() {
        this.inputEmail.setError(null);
        this.hideSoftKeyboard();
        this.inputPassword.setError(null);
        this.presenter.onLoginButtonClicked();
    }

    @Override
    public String getEmailAddress() {
        return this.editEmail.getText().toString();
    }

    @Override
    public String getPassword() {
        return this.editPassword.getText().toString();
    }

    @Override
    public void showEmailAddressError() {
        this.inputEmail.setError(this.getString(R.string.login_email_error));
    }

    @Override
    public void showPasswordError() {
        this.inputPassword.setError(this.getString(R.string.login_password_error));
    }

    @Override
    public void doOnLoginSuccess() {
        this.pref = getSharedPreferences("user_details", MODE_PRIVATE);
        SharedPreferences.Editor editor = this.pref.edit();
        editor.putString("usn",editEmail.getText().toString()).apply();
        editor.putString("pass",editPassword.getText().toString()).apply();
        editor.commit();
        this.hideProgressDialog();
        Intent mainActivityIntent = new Intent(this, load.class);
        this.startActivity(mainActivityIntent);
        this.finish();
    }

    @Override
    public void showLoginError() {
        this.hideProgressDialog();
        this.showDialogue(this,
                R.string.login_error_title,
                R.string.login_error_description,
                R.string.ok_label,
                () -> {});
    }

    @Override
    public void showLoginProgress() {
        this.showProgressDialog();
    }
}
