package co.za.dfmtechnologies.pestmonitoring;

public class Names1 {
    private String MB4000SprayBlockID ;
    private String ProductionUnit ;
    private  String BlockID;
    private String Number ;
    private String Code ;
    private String SprayGroup;
    private String Ha ;
    private String M2 ;
    private String Trees ;
    private String TreesPerHa ;
    private String PlantSpacing ;
    private String RowsPerRidge;
    private String DoubleRowWidth;
    private String TreeHeight;
    private String TreeWidth;
    private String TRVCalculated;
    private String TRV;
    private String UserField1;
    private String UserField2;
    private String UserField3;
    private String UserField4;
    private String UserField5;
    private String FarmID;
    private String CustomerID;
    private String Active;


    public Names1(String MB4000SprayBlockID,String ProductionUnit,String BlockID, String Number, String Code,String SprayGroup,String Ha,String M2, String Trees, String TreesPerHa, String PlantSpacing, String PlantRowWidth,String RowsPerRidge, String DoubleRowWidth , String TreeHeight, String TreeWidth, String TRVCalculated, String TRV ,
                  String UserField1, String UserField2,String UserField3,String UserField4,String UserField5, String FarmID, String CustomerID, String Active ) {
        this.MB4000SprayBlockID = MB4000SprayBlockID ;
        this.ProductionUnit = ProductionUnit;
        this.BlockID = BlockID;
        this.Number = Number ;
        this.Code = Code ;
        this.SprayGroup = SprayGroup ;
        this.Ha = Ha ;
        this.Trees = Trees ;
        this.TreesPerHa = TreesPerHa ;
        this.PlantSpacing = PlantSpacing ;
        this.RowsPerRidge = RowsPerRidge;
        this.DoubleRowWidth = DoubleRowWidth;
        this.TreeHeight = TreeHeight;
        this.TreeWidth = TreeWidth;
        this.TRVCalculated = TRVCalculated;
        this.TRV = TRV;
        this.UserField1 = UserField1;
        this.UserField2 = UserField2;
        this.UserField3 = UserField3;
        this.UserField4 = UserField4;
        this.UserField5 = UserField5;
        this.FarmID = FarmID;
        this.CustomerID = CustomerID;
        this.Active = Active;

    }



    public String getMB4000SprayBlockID() {
        return MB4000SprayBlockID;
    }

    public String getNumber() {
        return Number;
    }

    public String getCode() {
        return Code;
    }

    public String getHa() {
        return Ha;
    }

    public String getM2() {
        return M2;
    }

    public String getTrees() {
        return Trees;
    }

    public String getTreesPerHa() {
        return TreesPerHa;
    }

    public String getPlantSpacing() {
        return PlantSpacing;
    }

    public String getRowsPerRidge() {
        return RowsPerRidge;
    }

    public String getDoubleRowWidth() {
        return DoubleRowWidth;
    }

    public String getTreeHeight() {
        return TreeHeight;
    }

    public String getTreeWidth() {
        return TreeWidth;
    }

    public String getTRVCalculated() {
        return TRVCalculated;
    }

    public String getTRV() {
        return TRV;
    }

    public String getUserField1() {
        return UserField1;
    }

    public String getUserField2() {
        return UserField2;
    }

    public String getUserField3() {
        return UserField3;
    }

    public String getUserField4() {
        return UserField4;
    }

    public String getUserField5() {
        return UserField5;
    }

    public String getFarmID() {
        return FarmID;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public String getActive() {
        return Active;
    }

    public String getProductionUnit() {
        return ProductionUnit;
    }

    public String getBlockID() {
        return BlockID;
    }

    public String getSprayGroup() {
        return SprayGroup;
    }
}
