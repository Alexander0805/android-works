package co.za.dfmtechnologies.pestmonitoring.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import butterknife.ButterKnife;
import co.za.dfmtechnologies.R;
import co.za.dfmtechnologies.pestmonitoring.realm.DfmRealm;
import co.za.dfmtechnologies.pestmonitoring.rest.DfmRetrofit;
import io.reactivex.functions.Action;



/**
 * Base Activity
 */
public abstract class BaseActivity<P extends BasePresenter<V>, V extends BaseView> extends AppCompatActivity implements BaseView {

    private DfmRealm dfmRealm;
    private DfmRetrofit dfmRetrofit;
    private Dialog dialog;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.dfmRealm = DfmRealm.getInstance();
        this.dfmRetrofit = DfmRetrofit.getInstance();

        this.setContentView(this.getLayoutResId());
        this.createPresenter();

        P presenter = getPresenter();
        if (presenter != null) {
            presenter.attach(this.getBaseView());
        }

        ButterKnife.bind(this);

        this.setupViews();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        P presenter = getPresenter();
        if (presenter != null) {
            presenter.detach();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void showDialogue(@NonNull Context context,
                             int title,
                             int message,
                             int positiveButtonCaption,
                             @NonNull Action onPositiveClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        this.dialog = builder.setMessage(message)
                .setTitle(title)
                .setPositiveButton(positiveButtonCaption, (dialog1, which) -> {
                    try {
                        dialog1.dismiss();
                        onPositiveClickListener.run();
                    } catch (Exception e) {
                    }
                })
                .create();

        this.dialog.setCancelable(false);
        this.dialog.show();
    }

    public void hideDialogue() {
        if(this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }

    protected void showProgressDialog() {
        showProgressDialog(getString(R.string.please_wait));
    }

    protected void showProgressDialog(@NonNull String message) {
        hideProgressDialog();
        progressDialog = ProgressDialog.show(this, null, message, true, false);
    }

    public void hideProgressDialog() {
        if(this.progressDialog != null) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }

    protected void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if(inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }
    }






    private ActionBar setupActionBar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        return getSupportActionBar();
    }

    protected void showFragment(Class<? extends Fragment> fragment, boolean addToBackStack)
                    throws IllegalStateException, IllegalAccessException, InstantiationException {
        this.showFragment(fragment, null, addToBackStack, false);
    }

    protected void showFragment(Class<? extends Fragment> fragment, Bundle arguments, boolean addToBackStack, boolean removeCurrentFragment)
            throws IllegalStateException, IllegalAccessException, InstantiationException {

        FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();

        String fragmentTag = fragment.getSimpleName();

        Fragment fragmentToAdd;
        Fragment fragmentCached = this.getFragmentManager().findFragmentByTag(fragmentTag);
        if (fragmentCached != null) {
            fragmentToAdd = fragmentCached;
        } else {
            fragmentToAdd = fragment.newInstance();
        }

        if (arguments != null) {
            fragmentToAdd.setArguments(arguments);
        }

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragmentTag);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        } else {
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.disallowAddToBackStack();
        }



        fragmentTransaction.commitAllowingStateLoss();
        // Execute pending transactions here, so that the fragment can register to the message bus
        // before the activity sends messages to the fragment
        this.getFragmentManager().executePendingTransactions();
    }


    protected abstract
    @LayoutRes
    int getLayoutResId();

    protected abstract V getBaseView();

    protected abstract void createPresenter();

    @Nullable
    protected abstract P getPresenter();

    protected abstract void setupViews();

    public DfmRealm getDfmRealm() {
        return dfmRealm;
    }

    public DfmRetrofit getDfmRetrofit() {
        return dfmRetrofit;
    }
}
