package co.za.dfmtechnologies.pestmonitoring;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SprayblocksHelper  extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Mb4000offline.DB";
    public static final String TABLE_NAME = "SprayBlocks";
    public static final String TABLE_NAME1 = "MonitoringPoints";
    public static final String TABLE_NAME2 = "Pest";
    public static final String COL1="MB4000SprayBlockID";
    public static final String COL2="ProductionUnit";
    public static final String COL3="BlockID";
    public static final String COL4="Number";
    public static final String COL5="Code";
    public static final String COL6="SprayGroup";
    public static final String COL7="Ha";
    public static final String COL8="Trees";
    public static final String COL9="TreesPerHa";
    public static final String COL10="PlantSpacing";
    public static final String COL11="PlantRowWidth";
    public static final String COL12="RowsPerRidge";
    public static final String COL13="DoubleRowWidth";
    public static final String COL14="TreeHeight";
    public static final String COL15="TreeWidth";
    public static final String COL16="TRVCalculated";
    public static final String COL17="TRV";
    public static final String COL18="UserField1";
    public static final String COL19="UserField2";
    public static final String COL20="UserField3";
    public static final String COL21="UserField4";
    public static final String COL23="CustomerID";
    public static final String COL24="Active";



    public SprayblocksHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME +" (MB4000SprayBlockID TEXT,ProductionUnit TEXT, BlockID TEXT, Number TEXT, Code TEXT, SprayGroup TEXT, Ha TEXT, Trees TEXT, TreesPerHa TEXT,PlantSpacing TEXT, PlantRowWidth TEXT, RowsPerRidge TEXT, DoubleRowWidth TEXT, TreeHeight TEXT, TreeWidth TEXT, TRVCalculated TEXT, TRV TEXT, UserField1 TEXT, UserField2 TEXT, UserField3 TEXT, UserField4 TEXT, CustomerID TEXT , Active TEXT)");
        db.execSQL("create table " + TABLE_NAME1 +" (MB4000PestMonitoringPointID TEXT,PestMonitoringPoint TEXT, Code TEXT, Location TEXT, Pest TEXT, SprayBlockID TEXT, MonitoringPoints TEXT, CustomerID TEXT, Active TEXT)");
        db.execSQL("create table " + TABLE_NAME2 +" (Pest TEXT,TypeOfPest TEXT, Preditor TEXT, LinkToProduct TEXT, MonitorMethod TEXT, WithinGuidelines TEXT, Threshold TEXT, ThresholdDescription TEXT, ThresholdAction TEXT,LoggingMethod TEXT, CustomerID TEXT, Active TEXT)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME1);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME2);
        onCreate(db);
    }
    public boolean insertMdata(String mB4000PestMonitoringPointID,String pestMonitoringPoint,String code, String location, String pest,String sprayBlockID,String monitoringPoints, String customerID, String active ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues2 = new ContentValues();
        contentValues2.put("MB4000PestMonitoringPointID", mB4000PestMonitoringPointID);
        contentValues2.put("PestMonitoringPoint", pestMonitoringPoint);
        contentValues2.put("Code", code);
        contentValues2.put("Location", location);
        contentValues2.put("Pest", pest);
        contentValues2.put("SprayBlockID", sprayBlockID);
        contentValues2.put("MonitoringPoints", monitoringPoints);
        contentValues2.put("CustomerID", customerID);
        contentValues2.put("Active", active);


        long result = db.insert(TABLE_NAME1,null, contentValues2);
        if(result == -1)
            return false;
        else
            return true;
    }
    public boolean insertPData(String pest,String typeOfPest,String preditor, String linkToProduct, String monitorMethod,String withinGuidelines,String threshold, String thresholdDescription, String thresholdAction, String loggingMethod,String customerID, String active ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues3 = new ContentValues();
        contentValues3.put("Pest", pest);
        contentValues3.put("TypeOfPest", typeOfPest);
        contentValues3.put("Preditor", preditor);
        contentValues3.put("LinkToProduct", linkToProduct);
        contentValues3.put("MonitorMethod", monitorMethod);
        contentValues3.put("WithinGuidelines", withinGuidelines);
        contentValues3.put("Threshold", threshold);
        contentValues3.put("ThresholdDescription", thresholdDescription);
        contentValues3.put("ThresholdAction", thresholdAction);
        contentValues3.put("LoggingMethod", loggingMethod);
        contentValues3.put("CustomerID",customerID);
        contentValues3.put("Active", active);


        long result = db.insert(TABLE_NAME2,null, contentValues3);
        if(result == -1)
            return false;
        else
            return true;
    }


    public boolean insertData(String mB4000SprayBlockID,String productionUnit,String blockID ,String number, String code,String sprayGroup,String ha, String trees, String treesPerHa, String plantSpacing, String plantRowWidth,String rowsPerRidge, String doubleRowWidth , String treeHeight, String treeWidth, String tRVCalculated, String tRV ,
                              String userField1, String userField2,String userField3,String userField4, String customerID, String active ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, mB4000SprayBlockID);
        contentValues.put(COL2, productionUnit);
        contentValues.put(COL3, blockID);
        contentValues.put(COL4, number);
        contentValues.put(COL5, code);
        contentValues.put(COL6, sprayGroup);
        contentValues.put(COL7, ha);
        contentValues.put(COL8, trees);
        contentValues.put(COL9, treesPerHa);
        contentValues.put(COL10, plantSpacing);
        contentValues.put(COL11, plantRowWidth);
        contentValues.put(COL12, rowsPerRidge);
        contentValues.put(COL13, doubleRowWidth);
        contentValues.put(COL14, treeHeight);
        contentValues.put(COL15, treeWidth);
        contentValues.put(COL16, tRVCalculated);
        contentValues.put(COL17, tRV);
        contentValues.put(COL18, userField1);
        contentValues.put(COL19, userField2);
        contentValues.put(COL20, userField3);
        contentValues.put(COL21, userField4);
        contentValues.put(COL23, customerID);
        contentValues.put(COL24, active);


        long result = db.insert(TABLE_NAME,null, contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }


    /*
     * this method will give us all the name stored in sqlite
     * */
    public Cursor getNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME ;
        Cursor cursor = db.rawQuery(sql, null);
        return cursor;
    }
    public Cursor getMon(String [] numbers) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = null;
        c = db.rawQuery(
                "select number from "+ TABLE_NAME, numbers);
        return c;
    }

    /*
     * this method is for getting all the unsynced name
     * so that we can sync it with database
     * */
    public Cursor getUnsyncedNames() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + COL9 + " = 0;", null);
        return cursor;
    }



}
