package co.za.dfmtechnologies.pestmonitoring;
import android.Manifest;
import android.app.Notification;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;





import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import co.za.dfmtechnologies.R;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class traps extends AppCompatActivity  implements NetworkStateReceiver.NetworkStateReceiverListener {


    private NetworkStateReceiver networkStateReceiver;

    private static final String TAG = "0";
    private List list ;
    private ListView listView;
    private ListviewAdapter adpter;
    Button save,soffline,bblock;
    DatabaseHelper myDb;
    EditText cnt,pst;
    String spid;
    Intent intent;
    ImageView setting,home;
    TextView bn,mp;
    String imageString;
    SharedPreferences pref;
   String al,la,ur,cameraPermission[];
    SharedPreferences prf;
    ImageView mPreviewIv;
    final Handler handler = new Handler();
    Uri image_uri;
    SprayblocksHelper sDb;
    String cust;

    LocationManager locationManager;
    String latitude, longitude;
    private static final int REQUEST_LOCATION = 1,CAMERA_REQUEST_CODE = 200,IMAGE_PICK_CAMERA_CODE = 1001;
    private ArrayList<String> kaminey_dost_array_list = new ArrayList<String>();
    private ArrayList<String> monipest = new ArrayList<String>();
    private ArrayList<String> MPid = new ArrayList<String>();
    private ArrayList<String> Block = new ArrayList<>();
    private ArrayList<String> MPN = new ArrayList<String>();
    private NotificationUtils mNotificationUtils;
    NotificationCompat.Builder builder;
    NotificationManagerCompat notificationManager;
    int notificationId = 101;

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.traps);

        startNetworkBroadcastReceiver(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER )||!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
         showAlert();
        } else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER )||!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            checkLocation();
            getLocation();
        }
        getLocation();

        mNotificationUtils = new NotificationUtils(this);
        myDb = new DatabaseHelper((Context) this);
        sDb = new SprayblocksHelper((Context) this);

        getAllpest();
        prf = getSharedPreferences("user_details", MODE_PRIVATE);
        pref = getSharedPreferences("Login", MODE_PRIVATE);
        intent = new Intent(traps.this, blockView.class);
        al = prf.getString("mp", null);
        la = prf.getString("bn", null);
        ur = pref.getString("user", null);
        cust=prf.getString("cust",null);

        mp = findViewById(R.id.textView9);
        bn = findViewById(R.id.textView8);
        spid = prf.getString("spid", null);
        mp.setText("Monitoring Point: " +al);
        bn.setText("Block: "+la);

      bblock=findViewById(R.id.button9);
      bblock.setOnClickListener(new View.OnClickListener() {
         @Override
          public void onClick(View v) {
             locationManager.removeUpdates(locationListenerGPS);
             locationManager.removeUpdates(locationListenerNetwork);
             Intent intent = new Intent(traps.this, Blocks.class);
             traps.this.startActivity(intent);
          }
        });

        setting=(ImageView)findViewById(R.id.imageView6);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationManager.removeUpdates(locationListenerGPS);
                locationManager.removeUpdates(locationListenerNetwork);
                Intent intent = new Intent(traps.this, Setting.class);
                traps.this.startActivity(intent);
            }
        });
        home =(ImageView) findViewById(R.id.imageView5);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationManager.removeUpdates(locationListenerGPS);
                locationManager.removeUpdates(locationListenerNetwork);
                Intent intent = new Intent(traps.this, activity_pest_monitoring.class);
                traps.this.startActivity(intent);
            }
        });


        mPreviewIv=findViewById(R.id.imageIv);
        cnt = findViewById(R.id.editText1);
        pst = findViewById(R.id.editText);

        list = new ArrayList<Integer>();
        listView = (ListView) findViewById(R.id.listview);
        listView.setItemsCanFocus(true);
        for (int i = 0; i < getAllpest().size(); i++) {
            list.add("0");
        }


        adpter = new ListviewAdapter(this, list,getAllpest());
        listView.setAdapter(adpter);
        //camera permission
        cameraPermission = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        //storage permission
//      handler.postDelayed(new Runnable() {
//          @Override
//          public void run() {
//              if(networkcheck()==true){
//                  new RequestAsync().execute();
//                  new RequestAsync1().execute();
//              }
//              handler.postDelayed(this,5000 );
//          }
//      }, 5000);

        soffline = findViewById(R.id.button4);
        soffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pest = pst.getText().toString();
                String count = cnt.getText().toString();

                if (pest.equals("") || count.equals("")) {
                    Toast.makeText(traps.this, "Please add Pest or count before submitting", Toast.LENGTH_SHORT).show();
                } else  if(longitude.equals("")||longitude.equals("")){
                    Toast.makeText(traps.this, "Location not ready please wait...", Toast.LENGTH_SHORT).show();
                }
                else {

                    if (!checkCameraPermission()) {
                        //camera permission not allowed ,request it
                        requestCameraPermission();
                    } else {
                        //permission allowed, take picture

                        getLocation();
                        pickCamera();
                    }

                }
            }
        });


        save = findViewById(R.id.button5);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
                getLocation();

                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).toString().equals("")) {
                        Toast.makeText((Context) traps.this, list.get(i).toString() + "" + "has not been added", Toast.LENGTH_LONG).show();
                    } else {
                        String str = DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                        myDb.insertData(str, getMonitoringpointsid(al,getAllpest().get(i)).get(0),String.valueOf(list.get(i)) , getMonitoringpointsNumber(al,getAllpest().get(i)).get(0),spid,cust, "1",latitude,longitude,String.valueOf(getAllpest().get(i)));
                       // myDb.insertData(al, la, str, ur, String.valueOf(list.get(i)), String.valueOf(getAllpest().get(i)), "Counts Per Trap", 0);
                        MPid.clear();
                        MPN.clear();
                    }
                }
                Toast.makeText((Context) traps.this, "Reading Added", Toast.LENGTH_SHORT).show();
                getMonitoringpoints();
                Log.d("list size",String.valueOf(Block.size()));
                if(Block.size()>1){
                    locationManager.removeUpdates(locationListenerGPS);
                    locationManager.removeUpdates(locationListenerNetwork);
                    Intent intent = new Intent(traps.this, blockView.class);
                    traps.this.startActivity(intent);}else{
                    locationManager.removeUpdates(locationListenerGPS);
                    locationManager.removeUpdates(locationListenerNetwork);
                    Intent intent = new Intent(traps.this, Blocks.class);
                    traps.this.startActivity(intent);
                }
            }
        });
    }
    private boolean checkLocation() {
        if (!isLocationEnabled()){
            showAlert();}else{
        getLocation();}
        return isLocationEnabled();
    }
    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
    }
    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }
    public ArrayList<String> getMonitoringpoints() {



        try {
            SQLiteDatabase db = sDb.getWritableDatabase();


            //String sql ="Select PestMonitoringPoint From MonitoringPoints where SprayBlockID='"+spid+"'";
            String sql ="SELECT O.Number,I.PestMonitoringPoint ,O.MB4000SprayBlockID , P.Pest,P.LoggingMethod FROM [Sprayblocks] O JOIN MonitoringPoints I ON O.MB4000SprayBlockID = I.SprayBlockID JOIN Pest P ON P.Pest = I.Pest   where P.LoggingMethod =0 and I.SprayBlockID='"+spid+"' ORDER BY I.PestMonitoringPoint";
            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                if (Block.contains(cursor.getString(cursor.getColumnIndex("PestMonitoringPoint")))) {
                } else {
                    Block.add(cursor.getString(cursor.getColumnIndex("PestMonitoringPoint")));
                }

            }cursor.close();
            sDb.close();
        }catch(Exception ex){
            Log.e(TAG,"Error in getting points "+ex.toString());
        }

        return Block;
    }
    private void pickCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE,pst.getText().toString() );
        values.put(MediaStore.Images.Media.DESCRIPTION, "unlistedPest");
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);
    }


    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, cameraPermission, CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        boolean result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);

        return result;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean writeStorageAccepted = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted && writeStorageAccepted) {
                        pickCamera();
                    } else {
                        Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

        }
    }

    //handle image

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //got image from camera

        //     if (requestCode == IMAGE_PICK_CAMERA_CODE) {
        //got image from gallery now crop
        //         CropImage.activity(image_uri)
        //                 .setGuidelines(CropImageView.Guidelines.ON)// enabled image guide lines
        //                 .start(this);
        //  }

        //get image cropped image
        //    if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
        //      CropImage.ActivityResult result = CropImage.getActivityResult(data);
        //      if (resultCode == RESULT_OK){
        //     Uri resultUri = result.getUri();//get image uri
        //set image to view
        mPreviewIv.setImageURI(image_uri);

        BitmapDrawable bitmapDrawable = (BitmapDrawable) mPreviewIv.getDrawable();
        //get drawable bitmap for text recognition
        Bitmap bitmap = bitmapDrawable.getBitmap();
        //  ByteArrayOutputStream baos = new ByteArrayOutputStream();
//
        //  bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        //  byte[] imageBytes = baos.toByteArray();
        //
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, baos); // bm is the bitmap object
        byte[] b = baos.toByteArray();
        imageString =Base64.encodeToString(b,Base64.DEFAULT);
        //Log.d("base64",""+ imageString);
getLocation();

        String str = DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
     myDb.insertUlistedPest(str,"", cnt.getText().toString(), "1",latitude,longitude,pst.getText().toString(),imageString,spid,cust, "1",la);
        pst.setText("");
        cnt.setText("");
        locationManager.removeUpdates(locationListenerGPS);
        locationManager.removeUpdates(locationListenerNetwork);
        Toast.makeText(traps.this, "Unlisted Pest was Added ", Toast.LENGTH_SHORT).show();

        }
    public ArrayList<String> getAllpest() {


        ArrayList<String> kaminey_dost_array_list = new ArrayList<>();
        try {
            SQLiteDatabase db = sDb.getWritableDatabase();
          // String sql = "Select distinct sb.Pest,sb.LoggingMethod\n" +
          //         " from MonitoringPoints  as PMP Inner Join Pest as SB on pmp.Pest= sb.Pest and sb.LoggingMethod=0 and pmp.PestMonitoringPoint='"+al+"'";

            String sql="SELECT O.Number,I.PestMonitoringPoint,O.MB4000SprayBlockID ,\n" +
                    "       P.Pest,P.LoggingMethod,I.MB4000PestMonitoringPointID\n" +
                    "  FROM [Sprayblocks] O \n" +
                    "  JOIN MonitoringPoints I ON O.MB4000SprayBlockID = I.SprayBlockID \n" +
                    "  JOIN Pest P ON P.Pest = I.Pest   where P.LoggingMethod = 0 and I.PestMonitoringPoint='"+ al +"' and O.Number='"+ la +"'";
            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                kaminey_dost_array_list.add(cursor.getString(cursor.getColumnIndex("Pest")));
            }
            cursor.close();
            db.close();
        }catch(Exception ex){
            Log.e(TAG,"Error in getting Blocks "+ex.toString());
        }

        return kaminey_dost_array_list;
    }

    public ArrayList<String> getMonitoringpointsid(String a,String p) {


        try {
            SQLiteDatabase db = sDb.getWritableDatabase();

            String sql = "SELECT O.MB4000PestMonitoringPointID,\n" +
                    "       I.Pest,I.LoggingMethod\n" +
                    "  FROM MonitoringPoints O \n" +
                    "  JOIN Pest I ON I.Pest= O.Pest \n" +
                    "where O.PestMonitoringPoint='"+ a +"' and I.LoggingMethod=0 and O.pest= '"+p+"'";

            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                MPid.add(cursor.getString(cursor.getColumnIndex("MB4000PestMonitoringPointID")));
            }
            cursor.close();
            db.close();
        }catch(Exception ex){
            Log.e(TAG,"Error in getting points ID "+ex.toString());
        }

        return MPid;
    }
    public ArrayList<String> getMonitoringpointsNumber(String a,String p) {


        try {
            SQLiteDatabase db = sDb.getWritableDatabase();

            String sql = "SELECT O.MB4000PestMonitoringPointID,\n" +
                    "       I.Pest,I.LoggingMethod,\n" +
                    "       O.MonitoringPoints\n" +
                    "  FROM MonitoringPoints O \n" +
                    "  JOIN Pest I ON I.Pest= O.Pest \n" +
                    "where O.PestMonitoringPoint='"+ a +"' and I.LoggingMethod=0 and O.pest= '"+p+"'";

            Cursor cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                MPN.add(cursor.getString(cursor.getColumnIndex("MonitoringPoints")));
            }
            cursor.close();
            db.close();
        }catch(Exception ex){
            Log.e(TAG,"Error in getting points ID "+ex.toString());
        }

        return MPN;
    }



    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 50, 0,locationListenerNetwork);
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0 ,0, locationListenerGPS);


            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (location != null) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 50, 0,locationListenerNetwork);
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                latitude = String.valueOf(latti);
                longitude = String.valueOf(longi);




            } else if (location1 != null) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 1000 ,5, locationListenerGPS);
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                latitude = String.valueOf(latti);
                longitude = String.valueOf(longi);




            } else if (location2 != null) {
                locationManager.requestLocationUpdates(
                        LocationManager.PASSIVE_PROVIDER, 50 ,0, locationListenerGPS);
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                latitude = String.valueOf(latti);
                longitude = String.valueOf(longi);



            }
        }
    }


    public JSONArray getResults() {

        SQLiteDatabase myDataBase = myDb.getReadableDatabase();


        String searchQuery = "SELECT * FROM PestReading ";
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);


        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }

            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        myDb.close();
        Log.d("TAG_NAME", resultSet.toString());
        myDataBase.close();
        return resultSet;

    }
    public class RequestAsync extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {


            try {

                JSONObject object = new JSONObject();

                for (int i = 0; i < getResults().length(); i++) {

                    object = getResults().getJSONObject(i);

                    RequestHandler.sendPost1("http://probe.dfmsoftware.co.za/WebServices/MB4000PestMonitoringReadingsAndroid.ashx?Action=SaveMB4000PestMonitoringReadingsAndroid&", object);

                }

                updateNameStatus();

                return object.toString();

            } catch (Exception e) {


                return new String("Exception: " + e.getMessage());

            }

        }

        @Override
        protected void onPostExecute(String s) {
            if (s.contains("MB4000PestMonitoringReadingsID")) {

                String title = "Successful";
                String author = "Readings uploaded";

                if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
                    NotificationCompat.Builder nb = mNotificationUtils.
                            getAndroidChannelNotification(title, author);

                    mNotificationUtils.getManager().notify(101, nb.build());
                }

            }
        }

        public boolean updateNameStatus() {
            SQLiteDatabase db = myDb.getWritableDatabase();

            db.delete(myDb.TABLE_NAME, null, null);
            db.close();
            return true;
        }

    }

    public JSONArray getResults1() {

        SQLiteDatabase myDataBase = myDb.getReadableDatabase();


        String searchQuery = "SELECT * FROM PestReading1 ";
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);


        JSONArray resultSet = new JSONArray();
        JSONObject returnObj = new JSONObject();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }

            resultSet.put(rowObject);
            cursor.moveToNext();
        }

        cursor.close();
        myDb.close();
        Log.d("TAG_NAME", resultSet.toString());
        myDataBase.close();
        return resultSet;

    }


    public class RequestAsync1 extends AsyncTask<String, String, String> { //sending unlisted pest


        @Override
        protected String doInBackground(String... strings) {

            try {
                int PROGRESS_MAX = getResults1().length();
                int PROGRESS_CURRENT = 0;

                builder.setContentTitle("Pest Monitoring")
                        .setContentText("Upload in progress")
                        .setSmallIcon(R.drawable.dfmreview)
                        .setOnlyAlertOnce(true)
                        .setOngoing(true)
                        .setPriority(NotificationCompat.PRIORITY_LOW)
                        .setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
                notificationManager.notify(notificationId, builder.build());

                JSONObject object = new JSONObject();

                for (int i = 0; i < getResults1().length(); i++) {

                    int PROGRESS_CURRENT1 = i;

                    builder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT1, false);
                    notificationManager.notify(notificationId, builder.build());

                    object = getResults1().getJSONObject(i);


                    RequestHandler.sendPost1("http://probe.dfmsoftware.co.za/WebServices/MB4000PestMonitoringReadingsAndroid.ashx?Action=SaveMB4000PestMonitoringReadingsAndroid&", object);

                }

                updateNameStatus();


                return object.toString();

            } catch (Exception e) {


                return new String("Exception: " + e.getMessage());

            }

        }


        @Override
        protected void onPostExecute(String s) {



                builder.setContentText("Upload complete")
                        .setProgress(0, 0, false)
                        .setOngoing(false);
                notificationManager.notify(notificationId, builder.build());
                //     String title = "Successful";
                //     String author = "Readings uploaded";
//
                //     if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(author)) {
                //         NotificationCompat.Builder nb = mNotificationUtils.
                //                 getAndroidChannelNotification(title, author);
//
                //         mNotificationUtils.getManager().notify(101, nb.build());
                // }
//

        }

        public boolean updateNameStatus() {
            SQLiteDatabase db = myDb.getWritableDatabase();

            db.delete(myDb.TABLE_NAME1, null, null);
            db.close();
            return true;
        }

    }
 public boolean  networkcheck () {

     ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
     if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
             connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

         if( getResults().length()>0) {
             new RequestAsync().execute();
         }

         if( getResults1().length()>0) {
             new RequestAsync1().execute();
         }
         return true;
     } else
         return  false;
 }

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            double latti = location.getLatitude();
            double longi = location.getLongitude();
            latitude = String.valueOf(latti);
            longitude = String.valueOf(longi);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    //Toast.makeText(scouting.this, "Location Ready", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };



    private final LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {

            location.getAccuracy();
            double latti = location.getLatitude();
            double longi = location.getLongitude();
            latitude = String.valueOf(latti);
            longitude = String.valueOf(longi);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //Toast.makeText(scouting.this, "Network Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (intent == null) {
            intent = new Intent();
        }
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onPause() {
        /***/
        unregisterNetworkBroadcastReceiver(this);
        super.onPause();
   
    }

    @Override
    protected void onResume() {
        /***/
        registerNetworkBroadcastReceiver(this);

        super.onResume();

        if( getResults().length()>0) {
            new RequestAsync().execute();
        }

        if( getResults1().length()>0) {
            new RequestAsync1().execute();
        }


    }

    @Override
    public void networkAvailable() {
        Log.i(TAG, "networkAvailable()");

        if( getResults().length()>0) {
            new RequestAsync().execute();
        }

        if( getResults1().length()>0) {
            new RequestAsync1().execute();
        }

        //Proceed with online actions in activity (e.g. hide offline UI from user, start services, etc...)
    }

    @Override
    public void networkUnavailable() {
        Log.i(TAG, "networkUnavailable()");
        //Proceed with offline actions in activity (e.g. sInform user they are offline, stop services, etc...)
    }

    public void startNetworkBroadcastReceiver(Context currentContext) {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener((NetworkStateReceiver.NetworkStateReceiverListener) currentContext);
        registerNetworkBroadcastReceiver(currentContext);
    }

    /**
     * Register the NetworkStateReceiver with your activity
     * @param currentContext
     */
    public void registerNetworkBroadcastReceiver(Context currentContext) {
        currentContext.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     Unregister the NetworkStateReceiver with your activity
     * @param currentContext
     */
    public void unregisterNetworkBroadcastReceiver(Context currentContext) {
        currentContext.unregisterReceiver(networkStateReceiver);
    }
}
