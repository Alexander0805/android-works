package co.za.dfmtechnologies.pestmonitoring.ui.login;

import co.za.dfmtechnologies.pestmonitoring.ui.BasePresenter;
import co.za.dfmtechnologies.pestmonitoring.ui.BaseView;


/**
 * Login Contract
 */
public interface LoginContract {

    interface View extends BaseView {

        String getEmailAddress();

        String getPassword();

        void showEmailAddressError();

        void showPasswordError();

        void doOnLoginSuccess();

        void showLoginError();

        void showLoginProgress();
    }

    interface Presenter extends BasePresenter<View> {

        void onLoginButtonClicked();
    }
}
